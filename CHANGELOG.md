﻿# Changelog

Format:
%version%, %date%, %author%, %duration%:
%content%
%contentkeys%

### alpha 1.3.9, 31.03.2015, Tobi, 2h
Es gibt nun gurndsätzlich ein Menü, mit dem man das Spiel starten kann und in die Optionen, Achievemnets und Credits kann, jedoch ist davon noch nichts implemeniert.

 * Grundlagen Menü

### alpha 1.3.8, 31.03.2015, Tobi, 0.5h
Der Player kann nun 'rennen', also doppelt so schnell gehen und hat eine Schlaganimation für jeden WalkMode, welche jedoch im Moment noch die gleiche ist. Schlagen kann man nun über einen Linksklick und das Spell Rad wird durch einen Rechtsklick aufgerufen.

 * Move Dynamic Erweiterung
 * Steuerungsänderung

### alpha 1.3.7, 31.03.2015, Tobi, 0.5h
StandingW in Undead main ist nun texturiert.

 * Undead StandingW texturiert

### alpha 1.3.6, 30.03.2015, Tobi, 1h
StandingE in Undead main ist nun texturiert.

 * Undead StandingE texturiert

### alpha 1.3.5, 29.03.2015, Tobi, 6h
WalkingN in Undead main ist nun vollständig texturiert.

 * Undead WalkingN texturiert

### alpha 1.3.4, 29.03.2015, Tobi, 5h
WalkingS in Undead main ist nun vollständing texturiert.

 * Undead WalkingS texturiert

### alpha 1.3.3, 22.03.2015, Tobi, 0.5h
Die Kamerabewegung bei Schaden wird nun duch die Funktion f(x) = -41.53x⁴ + 88.4x³ - 60.57x² + 13.7x im Intervall [0;1] dargestellt.

 * Neue Schadensfunktion

### alpha 1.3.2, 22.03.2015, Tobi, 1h
Das Laden von Arrays funktioniert nun auch über Reflection in util.Util.getInstance(). So kann ein Entity nun beliebig viele TextureBoxes besitzen.

 * Arrays über Reflection

### alpha 1.3.1, 22.03.2015, Tobi, 3h
Das Spell-Rad funktioniert nun vollständig, so dass der Spieler nun darüber angreifen kann. Bisher jedoch dreht er sich beim Angriff nicht in die entsprechende Richtung und auch der Angriff selbst funktioniert weiterhin nicht vollständig. Die Fertigstellung des Spell-Rades liegt an der fertigen Strukturierung von Game und GUI, da beide nun eigene Packages haben und sich mittels eines GuiListeners bzw. GameListeners verständigen, den sie jeweils selbst darstellen. Somit kann z.B. Game über die Methode handlePlayerStats(int life) dem GameListener, also der GUI, mitteilen, wie viel Leben der Spieler hat. genau so funktioniert das Ganze mit Eingaben, die der Spieler über die GUI tätigt.

 * Spell-Rad (voll funktionsfähig)
 * Vollständige Umstrukturierung

### alpha 1.3.0, 22.03.2015, Tobi, 4h
Es gibt nun ein zu Hälfte funktionierendes Spell-Rad, mit dem man den Spell, mit welchem man angreifen möchte, auswählen kann. Die entsprechende Grafik wurde ebenfalls erstellt (wheel.xcf). Alle Grafiken der GUI liegen jetzt auch in einem Ordner. Außerdem wurde der Code nun so umstrukturiert, dass es einmal keine Level und Game Klassen mehr gibt, sondern nur noch eine Game Klasse, welche beide vereint und nun Main die beiden Klassen und Layer Game und GUI verwaltet. So werden in GUI nun Menüs, das Spell-Rad und alles andere angezeigt und in Game das Spiel selbst. GUI ist dabei für alle Eingaben zuständig auch diejenigen, welche von der Tastatur getätigt werden und teilt dann Game mit, was es zu tun hat. GUI ist dabei aus den in der vorherigen Version hinzugefügten GUI-Komponenten wie Layern und Buttons aufgebaut.

 * Spell-Rad (halb fertig)
 * Umstrukturierung

### alpha 1.2.9, 21.03.2015, Tobi, 1h
Es wurden einige Klassen für eine Grafische Benutzeroberfläche (GUI) hinzugefügt, um in Zukunft zu erlauben, dass Menüs, wie z.B. ein Hauptmenü oder das Inventar implementiert werden können.

 * Technische Möglichkeit für Menüs

### alpha 1.2.8, 19.03.2015, Tobi, 1.5
Es gibt nun ein Player-Model für die Kampfanimation beim Fireball-Spell. Außerdem wurde ein Bug im Programm behoben, welcher zur Folge hatte, dass das erste Frame jeder Kampfanimation für ein Update zu sehen war.

 * Wurf-Model Fireball
 * Bugfix bei Kampfanimation

### alpha 1.2.7, 19.03.2015, Tobi, 3h
Weiteren Fehler in WalkingW beim Undead Model behoben, bei dem die Arme entgegengesetzt ihrer eigentlichen Position waren. Außerdem wurde ein Bug bei WalkingS behoben, bei dem die Bilder vertauscht waren, so dass mit der falschen Animation gestartet wurde. Weiterhin wurde WalkingS fertig gestellt und einige kleine Änderungen bei WalkingN vorgenommen. Außerdem kann man sich nun nur noch in 4 Richtungen drehen, da die schrägen Animationen noch nicht fertig sind.

 * Bugfix in main.xcf (Arme falsch herum bei WalkingW)
 * Bugfix in main.xcf (WalkingS war falsch herum)
 * WalkingS model fertig
 * WalkingN model leicht abgeändert
 * Drehung in 4 Richtungen

### alpha 1.2.6, 18.03.2015, Tobi, 0.5h
Es gibt nun einen Debugmode, welcher durch die Taste 'F3' ein- bzw. ausgeschaltet werden kann. Dieser zeigt momentan nur die Boundingboxen der Entities an. Am Undead Model für das Laufen nach unten wurde weiter gemacht und es wurde ein Fehler in der Reihenfolge der Bilder berichtigt, welcher daraus resultiert ist, dass bei StandingW die Beine des Undead falsch herum waren.

 * Debugmode
 * Undead WalkingS Model weiter
 * Bugfix in main.xcf (Reihenfolge der Beine bei WalkingW)

### alpha 1.2.5, 18.03.2015, Niklas und Tobi, 7h
Es wurden einige Feuerball-Versuche unternommen und der Feuerball ist somit etwas weiter als in der Vorversion. Außerdem kann das Undead Model nun in alle Richtungen laufen, wobei das Laufen nach unten noch in Arbeit ist. Weiterhin drehen sich AttackEntities nun immer entsprechend ihrer Bewegungsrichtung, das Drehen selbst erfolgt jedoch weiterhin nur um den linken oberen Eckpunkt.

 * Feuerball Anfang
 * Undead Model (nicht ganz komplett)
 * Drehung der AttackEntities (noch falsch)

### alpha 1.2.4, 13.03.2015, Niklas und Tobi, 3h
Der Spieler rotiert nun richtig, das Angreifen wurde verbessert und der Anfang für eine Feuerball-Animation ist vorhanden. Außerdem wurde ein wenig an der Zombieanimation gearbeitet.

### alpha 1.2.3, 12.03.2015, Tobi, 0.5h
Der Schaden über SwordEntities funktioniert nun komplett und es wurde eine Länge der Animation in move.txt hinzugefügt, damit diese nun auch aufhört. Außerdem spastet die Framerate im Titel nicht mehr so rum und aktualisiert nur alle 0,25 Sekunden.

### alpha 1.2.2, 12.03.2015, Tobi, 1.5
Das Spawnen und die Funktion von SwordEntities wurde verbessert, so dass man nun beim Klicken Schaden machen kann.

### alpha 1.2.1, 11.03.2015, Tobi, 0.5h
Man kann nun Entities bzw. Texturen rotieren.

### alpha 1.2.0, 10.03.2015, Tobi, 1h
Es gibt nun eine einfache KI, welche dem Spieler folgen kann. Außerdem wurde das System, mit welchem entschieden wird in welche Richtung man guckt (North, North East, East, ...), berichtigt, da nun alle Bereiche in denen eine Richtung gilt gleich groß (22,5°) sind.

### alpha 1.1.4, 10.03.2015, Tobi, 3h
Fertigstellung des Models der Undead WalkingE Animation. Außerdem wurde hinzugefügt, dass in move.txt nun die sogenannte 'drawOrder' angegeben werden kann, also die Reihenfolge in welcher die einzelnen Texturen gezeichnet werden. Dies geschieht über die Indexe, welche den Texturen nun in texture.txt gegeben werden. Weiterhin wurde ansatzweise implementiert, dass sich TextureBoxes bei einer neuen Animation synchronisieren, um nicht verschoben abzulaufen.

### alpha 1.1.3, 09.02.2015, Tobi, 2h
Überarbeitung der Undead Standanimation und Anfang der WalkingE Animation.

### alpha 1.1.2, 08.03.2015, Niklas und Tobi, 2h
Festlegung der Ziele bis zum nästen Video und generelle Besprechung Kampfsystems.

### alpha 1.1.1, 07.03.2015, Tobi, 3h
Es werden nun bei den Angriffsanmationen SwordEntities gespawnt. Dies geschieht über eine Methode der neuen Klasse Util, welche eine Sammlung für statische Methoden darstellen soll. Diese Methode getInstance(String str) instanziert aus einem Sting wie z.B. "SwordEntity(Bounds(0.0,0.0,0,0,0,0))" er Refelction das Objekt, was entsprechend der Java-Syntax entstehen würde. Die SwordEntities werden noch nicht mit dem Spieler bewegt und spawnen absolut. Dabei musste das Spawning und Respawning System ein wenig verbessert werden, um zu verhindern, dass mehrere Programmteile gleichzeitg auf die Entitylisten zugreifen.
Was ein Krampf! #Reflectionisscheiße

### alpha 1.1.0, 07.03.2015, Tobi, 4.5h
Alle Bewegungs-/Stand-/Schlag- und sonstige Animationen werden aus Ordnern zu der entsprechenden Kreatur gelesen. So muss einem Creature Entity nur noch der Ordnername und die Bounds übergeben werden. Dazu zählt auch die Bewegungsgeschwindigkeit und die SwordEntities, die beim Angriff gespawnt werden (Angriffanimationen sind noch nicht implementiert). Dies sieht wie folgt aus: Es gibt eine textures.txt, welche alle Texturen der Kreatur namentlich enthält. Zu jeder Textur gehört dann eine %name%.png und %name%.txt. Die %name%.png enthält wie gewöhnlich die Einzeltexturen und die %name%.txt enthält die Beschreibung, welche Einzeltexturen welche Animation bilden. Die Animationen mit Movementspeed und SwordEntities sind in der move.txt beschrieben.

### alpha 1.0.0, 05.03.2015, Alle, 67h
Alles ^^
