#!/bin/sh

folder=lib
file=/tmp/slick.zip

# Downloads the jar to the library folder
mkdir -p "$folder"
wget http://slick.ninjacave.com/slick.zip -O "$file"
unzip "$file" -x "javadoc/* scripts/* src/* testdata/* tools/* Pack200Task.jar" -d "$folder"
#rm "$file"
