# TheProlog

A Game for the [Game++ Community Challenge #6](https://youtu.be/PiHY_DNTyKI) by [LetsGameDev](https://www.youtube.com/user/Tomzalat)

![Imgur](https://i.imgur.com/9n3SvVP.png)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/iBSoNdHwt3Q" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Setting up LWJGL and Slick

Run the [set_up_lwjgl.sh](set_up_lwjgl.sh) script

```sh
$ sh set_up_lwjgl.sh
```

Or download Slick manually from the [Slick website](http://slick.ninjacave.com/) and create a folder `lib` to extract the zip file into.
