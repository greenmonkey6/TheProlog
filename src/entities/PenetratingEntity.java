package entities;

import util.Bounds;
import util.TextureBox;

public class PenetratingEntity extends MoveableEntity {
	public PenetratingEntity(Bounds bounds, TextureBox... textureBoxes) {
		super(bounds, textureBoxes);
	}

	@Override
	protected void collisionReaction(Entity entity) {
		// No Reaction
	}
}
