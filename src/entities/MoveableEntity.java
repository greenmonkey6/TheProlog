package entities;

import util.Bounds;
import util.TextureBox;

public abstract class MoveableEntity extends Entity {
	private double directionX, directionY;
	private double speed;

	public MoveableEntity(Bounds bounds, TextureBox... textureBoxes) {
		super(bounds, textureBoxes);

		directionX = 0;
		directionY = 1;

		speed = 0;
	}

	@Override
	public void update(int delta) {
		getBounds().addX(speed * directionX * delta);
		getBounds().addY(speed * directionY * delta);

		super.update(delta);
	}

	public void setDirection(double directionX, double directionY) {
		double length = Math.sqrt(directionX * directionX + directionY
				* directionY);
		setDirectionAsUnitVector(directionX / length, directionY / length);
	}

	public void setDirectionAsUnitVector(double directionX, double directionY) {
		this.directionX = directionX;
		this.directionY = directionY;
	}

	public double getDirectionX() {
		return directionX;
	}

	public double getDirectionY() {
		return directionY;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public double getSpeed() {
		return speed;
	}

	public void collide(Entity entity) {
		if (entity != this && entity.isPassable()) {
			if (getBounds().intersects(entity.getBounds())) {
				if (!(entity instanceof MoveableEntity)) {
					collisionReaction(entity);
				}

				if (entity instanceof Attacking) {
					((Attacking) entity).attack(this);
				}
			}
		}
	}

	protected abstract void collisionReaction(Entity entity);
}
