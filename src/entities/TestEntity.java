package entities;

import util.AnimatedTextureBox;
import util.Bounds;

public class TestEntity extends AttackEntity {
	static AnimatedTextureBox texture = new AnimatedTextureBox(
			"fireballMeteorArtSpell", 0, -16, 16, 1);

	static {
		texture.setAnim(0, 0, 10, AnimatedTextureBox.HORIZONTAL, 50);
	}

	public TestEntity() {
		super(new Bounds(890, 550, 16, 16, 0, 96), 50, texture);
		setDirection(1, 0);
		setSpeed(0.05);
	}
}
