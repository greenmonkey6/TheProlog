package entities;

import util.Bounds;
import util.TextureBox;

public class UnmoveableEntity extends Entity {
	public UnmoveableEntity(Bounds bounds, TextureBox... textureBoxes) {
		super(bounds, textureBoxes);
	}
}
