package entities;

import util.Bounds;
import util.TextureBox;

public abstract class CollidableEntity extends MoveableEntity {
	protected final int LEFT = 0x0;
	protected final int RIGHT = 0x1;
	protected final int UP = 0x2;
	protected final int DOWN = 0x3;

	public CollidableEntity(Bounds bounds, TextureBox[] textureBoxes) {
		super(bounds, textureBoxes);
	}

	@Override
	protected void collisionReaction(Entity entity) {
		int move = 0;

		double x1 = getBounds().getX();
		double y1 = getBounds().getY();
		int w1 = getBounds().getWidth();
		int h1 = getBounds().getHeight();
		double dx1 = getDirectionX();
		double dy1 = getDirectionY();

		double x2 = entity.getBounds().getX();
		double y2 = entity.getBounds().getY();
		int w2 = entity.getBounds().getWidth();
		int h2 = entity.getBounds().getHeight();

		if (dx1 == 0) {
			if (dy1 > 0) { // Oben
				move = UP;
			} else if (dy1 < 0) { // Unten
				move = DOWN;
			}
		} else if (dy1 == 0) {
			if (dx1 > 0) { // Links
				move = LEFT;
			} else if (dx1 < 0) { // Rechts
				move = RIGHT;
			}
		} else {
			double mx = x1 + w1 / 2;
			double my = y1 + h1 / 2;

			if (dx1 < 0) {
				if (dy1 < 0) { // Unten rechts
					if (isPointRightOfVector(dx1, dy1, x2 + w2, y2 + h2, mx, my)) {
						move = RIGHT;
					} else {
						move = DOWN;
					}
				} else if (dy1 > 0) { // Oben rechts
					if (isPointRightOfVector(dx1, dy1, x2 + w2, y2, mx, my)) {
						move = UP;
					} else {
						move = RIGHT;
					}
				}
			} else if (dx1 > 0) {
				if (dy1 < 0) { // Unten links
					if (isPointRightOfVector(dx1, dy1, x2, y2 + h2, mx, my)) {
						move = DOWN;
					} else {
						move = LEFT;
					}
				} else if (dy1 > 0) { // Oben links
					if (isPointRightOfVector(dx1, dy1, x2, y2, mx, my)) {
						move = LEFT;
					} else {
						move = UP;
					}
				}
			}
		}

		moveOutOfEntity(entity, move);
	}

	protected abstract void moveOutOfEntity(Entity entity, int move);

	private boolean isPointRightOfVector(double dx, double dy, double ex,
			double ey, double px, double py) {
		return (-dy * (px - ex) + dx * (py - ey) > 0);
	}
}
