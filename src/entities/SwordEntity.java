package entities;

import util.Bounds;

public class SwordEntity extends PenetratingEntity implements Attacking {
	private int damage;
	private boolean damaging;
	private int damageAfter;
	private boolean lock;

	public SwordEntity(Bounds bounds, int damage, int damageAfter) {
		super(bounds);
		this.damage = damage;
		damaging = false;
		this.damageAfter = damageAfter;
		lock = false;
	}

	@Override
	public void update(int delta) {
		super.update(delta);

		if (damaging) {
			damaging = false;
		}
	}

	public void damageOn() {
		if (!lock) {
			damaging = true;
			lock = true;
		}
	}

	public void unlock() {
		lock = false;
	}

	public int getDamageAfter() {
		return damageAfter;
	}

	@Override
	public void attack(Entity entity) {
		if (damaging) {
			if (entity instanceof CreatureEntity) {
				((CreatureEntity) entity).hurt(damage, 0, 0);
			}
		}
	}
}
