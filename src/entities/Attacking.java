package entities;

public interface Attacking {
	public void attack(Entity entity);
}
