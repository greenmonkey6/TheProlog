package entities;

import util.Bounds;
import util.TextureBox;

public class AttackEntity extends ElasticEntity implements Attacking {
	private int wallCounter;

	private int damage;

	public AttackEntity(Bounds bounds, int damage, TextureBox... textureBoxes) {
		super(bounds, textureBoxes);

		this.damage = damage;
		wallCounter = 0;
	}

	@Override
	public void update(int delta) {
		int angle = (int) (Math.acos(getDirectionX()) * 180 / Math.PI) - 90;
		if (getDirectionY() < 0 ) {
			angle = -angle + 180;
		}
		setAngle(angle);
		super.update(delta);
	}

	@Override
	protected void moveOutOfEntity(Entity entity, int move) {
		wallCounter++;

		if (wallCounter >= 20) {
			despawn();
		}

		super.moveOutOfEntity(entity, move);
	}

	@Override
	public void attack(Entity entity) {
		if (entity instanceof CreatureEntity) {
			((CreatureEntity) entity).hurt(damage, getDirectionX(),
					getDirectionY());

			despawn();
		}
	}
}
