package entities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import creatures.Player;
import util.AnimatedTextureBox;
import util.Bounds;
import util.FileAnimatedTextureBox;
import util.TextureBox;
import util.Util;

public class CreatureEntity extends InelasticEntity {
	public static final int STANDING = 0x0;
	public static final int WALKING = 0x1;
	public static final int RUNNING = 0x2;

	public static final int PEACEFUL = 0x3;
	public static final int PUNCH = 0x4;
	public static final int FIRE_BALL = 0x6;

	public static final int NORTH = 0x0;
	public static final int NORTH_EAST = 0x1;
	public static final int EAST = 0x2;
	public static final int SOUTH_EAST = 0x3;
	public static final int SOUTH = 0x4;
	public static final int SOUTH_WEST = 0x5;
	public static final int WEST = 0x6;
	public static final int NORTH_WEST = 0x7;

	public static final int ORIENTATIONS = 8;

	public static final int KI_STANDING = 0x0;
	public static final int KI_WALING_AROUND = 0x1;
	public static final int KI_ATTACKING_PLAYER = 0x2;

	boolean test = false;

	private int animationValue;
	private MovementAnimation[][] animations;

	private ArrayList<SwordEntity> swordEntities;
	private ArrayList<AttackEntity> attackEntities;

	private int walkMode;
	private int fightMode;
	private int orientation;

	private double damageTimer;

	private int turnAnimTimer;
	private int turnAnimDuration = 70;

	private int kiMode;

	private int life;

	public CreatureEntity(Bounds bounds, String name) {
		super(bounds);

		ArrayList<TextureBox> textureBoxes;
		textureBoxes = new ArrayList<TextureBox>();

		try {
			BufferedReader reader;
			reader = new BufferedReader(new FileReader(new File(
					"res/creatures/" + name + "/anims.txt")));

			int largest = 0;
			for (String currentLine = reader.readLine(); currentLine != null; currentLine = reader
					.readLine()) {
				int temp = Integer.parseInt(new StringTokenizer(currentLine,
						" ", false).nextToken());
				if (temp > largest) {
					largest = temp;
				}
			}
			animations = new MovementAnimation[largest + 1][ORIENTATIONS];

			reader.close();

			reader = new BufferedReader(new FileReader(new File(
					"res/creatures/" + name + "/move.txt")));

			for (String currentLine = reader.readLine(); currentLine != null; currentLine = reader
					.readLine()) {
				int value, orientation;

				StringTokenizer tok = new StringTokenizer(currentLine, " ",
						false);
				value = Integer.parseInt(tok.nextToken());
				String temp = tok.nextToken();
				if (temp.equals("N")) {
					orientation = CreatureEntity.NORTH;
				} else if (temp.equals("NE")) {
					orientation = CreatureEntity.NORTH_EAST;
				} else if (temp.equals("E")) {
					orientation = CreatureEntity.EAST;
				} else if (temp.equals("SE")) {
					orientation = CreatureEntity.SOUTH_EAST;
				} else if (temp.equals("S")) {
					orientation = CreatureEntity.SOUTH;
				} else if (temp.equals("SW")) {
					orientation = CreatureEntity.SOUTH_WEST;
				} else if (temp.equals("W")) {
					orientation = CreatureEntity.WEST;
				} else if (temp.equals("NW")) {
					orientation = CreatureEntity.NORTH_WEST;
				} else {
					orientation = -1;
				}

				double movementSpeed;
				int type;
				int duration = 0;
				ArrayList<SwordEntity> swordEntities = new ArrayList<SwordEntity>();
				ArrayList<AttackEntity> attackEntities = new ArrayList<AttackEntity>();
				ArrayList<Integer> drawOrder = new ArrayList<Integer>();

				movementSpeed = Double.parseDouble(tok.nextToken());
				temp = tok.nextToken();
				if (temp.equals("infinity")) {
					duration = MovementAnimation.INFINITY;
				} else {
					duration = Integer.parseInt(temp);
				}

				type = -1;
				boolean drawOrderLoop = true;
				while (drawOrderLoop) {
					drawOrderLoop = false;
					temp = tok.nextToken();
					if (temp.equals("move")) {
						type = MovementAnimation.MOVE;
					} else if (temp.equals("attack")) {
						type = MovementAnimation.ATTACK;
						while (tok.hasMoreTokens()) {
							Object object = Util.getInstance(tok.nextToken());
							if (object instanceof Entity) {
								((Entity) object)
										.setEntitySpawnListener(getEntitySpawnListener());
							}
							if (object instanceof SwordEntity) {
								swordEntities.add((SwordEntity) object);
							} else if (object instanceof AttackEntity) {
								attackEntities.add((AttackEntity) object);
							}
						}
					} else {
						drawOrder.add(Integer.parseInt(temp));
						drawOrderLoop = true;
					}
				}

				SwordEntity[] swordEntitiesArray = new SwordEntity[swordEntities
						.size()];
				for (int i = 0; i < swordEntitiesArray.length; i++) {
					swordEntitiesArray[i] = swordEntities.get(i);
				}

				AttackEntity[] attackEntitiesArray = new AttackEntity[attackEntities
						.size()];
				for (int i = 0; i < attackEntitiesArray.length; i++) {
					attackEntitiesArray[i] = attackEntities.get(i);
				}

				int[] drawOrderArray = new int[drawOrder.size()];
				for (int i = 0; i < drawOrderArray.length; i++) {
					drawOrderArray[i] = drawOrder.get(i);
				}

				animations[value][orientation] = new MovementAnimation(
						movementSpeed, duration, swordEntitiesArray,
						attackEntitiesArray, drawOrderArray);
			}

			reader.close();

			reader = new BufferedReader(new FileReader(new File(
					"res/creatures/" + name + "/textures.txt")));

			for (String currentLine = reader.readLine(); currentLine != null; currentLine = reader
					.readLine()) {
				StringTokenizer tok = new StringTokenizer(currentLine, " ",
						false);

				int index = Integer.parseInt(tok.nextToken());
				FileAnimatedTextureBox textureBox = new FileAnimatedTextureBox(
						name, tok.nextToken());
				textureBoxes.add(index, textureBox);
				if (tok.hasMoreElements() && tok.nextToken().equals("syncWith")) {
					textureBox
							.setSynchronizedTextureBox((AnimatedTextureBox) textureBoxes
									.get(Integer.parseInt(tok.nextToken())));
				}
			}

			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		TextureBox[] textureBoxesArray = new TextureBox[textureBoxes.size()];
		for (int i = 0; i < textureBoxesArray.length; i++) {
			textureBoxesArray[i] = textureBoxes.get(i);
		}

		setTextureBoxes(textureBoxesArray);

		swordEntities = new ArrayList<SwordEntity>();
		attackEntities = new ArrayList<AttackEntity>();

		walkMode = STANDING;
		fightMode = PEACEFUL;
		orientation = SOUTH;

		damageTimer = 0;

		turnAnimTimer = 0;
		turnAnimDuration = 70;

		kiMode = KI_STANDING;

		life = 100;
	}

	@Override
	public void update(int delta) {
		if (life <= 0) {
			despawn();
		}

		int oldOrientation = orientation;
		int oldAnimationValue = animationValue;

		// Orientation berechnen
		double x = getDirectionX();
		double y = getDirectionY();

		int targetOrientation = 0;

		// if (x > Math.cos(22.5 / 180 * Math.PI)) {
		// targetOrientation = EAST;
		// } else if (x > Math.cos(67.5 / 180 * Math.PI)) {
		// if (y < 0) {
		// targetOrientation = NORTH_EAST;
		// } else {
		// targetOrientation = SOUTH_EAST;
		// }
		// } else if (x > Math.cos(112.5 / 180 * Math.PI)) {
		// if (y < 0) {
		// targetOrientation = NORTH;
		// } else {
		// targetOrientation = SOUTH;
		// }
		// } else if (x > Math.cos(157.5 / 180 * Math.PI)) {
		// if (y < 0) {
		// targetOrientation = NORTH_WEST;
		// } else {
		// targetOrientation = SOUTH_WEST;
		// }
		// } else {
		// targetOrientation = WEST;
		// }

		if (x >= Math.cos(45.0 / 180 * Math.PI)) {
			targetOrientation = EAST;
		} else if (x > Math.cos(135.0 / 180 * Math.PI)) {
			if (y < 0) {
				targetOrientation = NORTH;
			} else {
				targetOrientation = SOUTH;
			}
		} else {
			targetOrientation = WEST;
		}

		int turnDirection;
		int orientationDif = targetOrientation - orientation;

		if (orientationDif >= 4) {
			orientationDif -= 8;
		} else if (orientationDif < -4) {
			orientationDif += 8;
		}

		if (orientationDif > 0) {
			turnDirection = 1;
		} else if (orientationDif < 0) {
			turnDirection = -1;
		} else {
			turnDirection = 0;
		}

		if (turnDirection != 0) {
			turnAnimTimer += delta;

			if (turnAnimTimer >= turnAnimDuration) {
				int value = turnAnimTimer / turnAnimDuration;
				orientation += value * turnDirection;
				turnAnimTimer -= value * turnAnimDuration;
			}

			if (orientation >= ORIENTATIONS) {
				orientation = orientation % ORIENTATIONS;
			} else if (orientation < 0) {
				orientation = orientation + ORIENTATIONS;
			}
		} else {
			turnAnimTimer = turnAnimDuration;
		}

		// Animation setzen (UNTEN NACHMAL!)
		if (fightMode == PEACEFUL) {
			animationValue = walkMode;
		} else if (fightMode == PUNCH) {
			animationValue = 3 + walkMode;
		} else if (fightMode == FIRE_BALL) {
			animationValue = 6;
		}

		// Angriff
		damageTimer += delta;

		for (SwordEntity swordEntity : swordEntities) {
			if (damageTimer >= swordEntity.getDamageAfter()) {
				swordEntity.damageOn();
			}
		}

		if (damageTimer >= animations[animationValue][orientation]
				.getDuration()) {
			fightMode = PEACEFUL;
			damageTimer = 0;
			for (SwordEntity swordEntity : swordEntities) {
				swordEntity.unlock();
			}
		}

		// Animation setzen (OBEN NOCHMAL!)
		if (fightMode == PEACEFUL) {
			animationValue = walkMode;
		} else if (fightMode == PUNCH) {
			animationValue = 3 + walkMode;
		} else if (fightMode == FIRE_BALL) {
			animationValue = 6;
		}

		if (oldAnimationValue != animationValue
				|| oldOrientation != orientation) {
			// Alle SwordEnities despawnen
			for (SwordEntity swordEntity : swordEntities) {
				getEntitySpawnListener().despawnEntity(swordEntity);
			}
			swordEntities.clear();

			// SwordEntities spawnen
			for (SwordEntity swordEntity : animations[animationValue][orientation]
					.getSwordEntities()) {
				swordEntities.add(swordEntity);
				getEntitySpawnListener().spawnEntity(swordEntity);
			}
		}

		if (!test) {
			for (AttackEntity attackEntity : animations[animationValue][orientation]
					.getAttackEntities()) {
				attackEntities.add(attackEntity);
				getEntitySpawnListener().spawnEntity(attackEntity);
				test = true;
			}
		}
		// Speed setzen
		setSpeed(animations[animationValue][orientation].getMovementSpeed());

		// DrawOrder setzen
		setDrawOrder(animations[animationValue][orientation].getDrawOrder());

		// TextureBoxes synchronisieren
		if (oldAnimationValue != animationValue
				|| oldOrientation != orientation) {
			for (TextureBox textureBox : getTextureBoxes()) {
				((AnimatedTextureBox) textureBox).sync();
			}
		}

		// Animation der textureBoxes setzen
		for (TextureBox textureBox : getTextureBoxes()) {
			((FileAnimatedTextureBox) textureBox).setAnim(animationValue,
					orientation);
		}

		super.update(delta);
	}

	public void setWalkMode(int walkMode) {
		this.walkMode = walkMode;
	}

	public void setTurnAnimDuration(int turnAnimDuration) {
		this.turnAnimDuration = turnAnimDuration;
	}

	public double getMidX() {
		return getBounds().getX() + getBounds().getWidth() / 2;
	}

	public double getMidY() {
		return getBounds().getY() + getBounds().getHeight() / 2;
	}

	public void attack(int fightMode) {
		this.fightMode = fightMode;
	}

	public void hurt(int damage, double directionX, double directionY) {
		life -= damage;
	}

	public int getLife() {
		return life;
	}

	public void ki(ArrayList<Entity> entities) {
		switch (kiMode) {
		case KI_STANDING:
			// nothing
			break;

		case KI_WALING_AROUND:
			// TODO
			break;

		case KI_ATTACKING_PLAYER:
			ArrayList<Player> players = new ArrayList<Player>();
			for (Entity entity : entities) {
				if (entity instanceof Player) {
					players.add((Player) entity);
				}
			}

			for (Player player : players) {
				double playerX = player.getBounds().getX()
						+ player.getBounds().getWidth() / 2;
				double playerY = player.getBounds().getY()
						+ player.getBounds().getHeight() / 2;
				double x = getBounds().getX() + getBounds().getWidth() / 2;
				double y = getBounds().getY() + getBounds().getHeight() / 2;

				setDirection(playerX - x, playerY - y);
				setWalkMode(WALKING);
			}
			break;

		default:
			break;
		}
	}

	public void setKiMode(int kiMode) {
		this.kiMode = kiMode;
	}

	private class MovementAnimation {
		public static final int MOVE = 0x0;
		public static final int ATTACK = 0x1;

		public static final int INFINITY = -1;

		private double movementSpeed;
		private int duration;
		private SwordEntity[] swordEntities;
		private AttackEntity[] attackEntities;
		private int[] drawOrder;

		public MovementAnimation(double movementSpeed, int duration,
				SwordEntity[] swordEntities, AttackEntity[] attackEntities,
				int[] drawOrder) {
			this.movementSpeed = movementSpeed;
			this.duration = duration;
			this.swordEntities = swordEntities;
			this.attackEntities = attackEntities;
			this.drawOrder = drawOrder;
		}

		public double getMovementSpeed() {
			return movementSpeed;
		}

		public int getDuration() {
			return duration;
		}

		public SwordEntity[] getSwordEntities() {
			return swordEntities;
		}

		public AttackEntity[] getAttackEntities() {
			return attackEntities;
		}

		public int[] getDrawOrder() {
			return drawOrder;
		}
	}
}
