package entities;

import util.Bounds;
import util.TextureBox;

public class InelasticEntity extends CollidableEntity {
	public InelasticEntity(Bounds bounds, TextureBox... textureBoxes) {
		super(bounds, textureBoxes);
	}

	@Override
	protected void moveOutOfEntity(Entity entity, int move) {
		switch (move) {
		case LEFT:
			getBounds()
					.setX(entity.getBounds().getX() - getBounds().getWidth());
			break;
		case RIGHT:
			getBounds().setX(
					entity.getBounds().getX() + entity.getBounds().getWidth());
			break;
		case UP:
			getBounds().setY(
					entity.getBounds().getY() - getBounds().getHeight());
			break;
		case DOWN:
			getBounds().setY(
					entity.getBounds().getY() + entity.getBounds().getHeight());
			break;
		}
	}
}
