package entities;

import util.Bounds;
import util.TextureBox;

public class ElasticEntity extends CollidableEntity {
	public ElasticEntity(Bounds bounds, TextureBox... textureBoxes) {
		super(bounds, textureBoxes);
	}

	@Override
	protected void moveOutOfEntity(Entity entity, int move) {
		switch (move) {
		case LEFT:
			setDirection(-getDirectionX(), getDirectionY());
			break;
		case RIGHT:
			setDirection(-getDirectionX(), getDirectionY());
			break;
		case UP:
			setDirection(getDirectionX(), -getDirectionY());
			break;
		case DOWN:
			setDirection(getDirectionX(), -getDirectionY());
			break;
		}
	}
}
