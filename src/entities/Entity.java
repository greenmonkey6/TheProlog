package entities;

import util.Bounds;
import util.EntitySpawnListener;
import util.NoListenerException;
import util.TextureBox;

public abstract class Entity {
	public static boolean showBounds = false;

	private EntitySpawnListener spawnListener;

	private Bounds bounds;
	private TextureBox[] textureBoxes;
	private int angle;

	private int[] drawOrder;

	public Entity(Bounds bounds, TextureBox... textureBoxes) {
		this.bounds = bounds;
		setTextureBoxes(textureBoxes);
	}

	public void render(double cameraX, double cameraY, int zoom) {
		for (int i = 0; i < drawOrder.length; i++) {
			textureBoxes[drawOrder[i]].render(cameraX, cameraY, zoom);
		}

		if (showBounds) {
			bounds.render(cameraX, cameraY, zoom);
		}
	}

	public void update(int delta) {
		for (TextureBox textureBox : textureBoxes) {
			textureBox.setPosX(getBounds().getX());
			textureBox.setPosY((getBounds().getY() + getBounds().getHeight()));
			textureBox.setAngle(angle);
			textureBox.update(delta);
		}
	}

	public Bounds getBounds() {
		return bounds;
	}

	public void setTextureBoxes(TextureBox... textureBoxes) {
		this.textureBoxes = textureBoxes;

		drawOrder = new int[textureBoxes.length];
		for (int i = 0; i < drawOrder.length; i++) {
			drawOrder[i] = i;
		}
	}

	protected TextureBox[] getTextureBoxes() {
		return textureBoxes;
	}

	public void setDrawOrder(int[] drawOrder) {
		this.drawOrder = drawOrder;
	}

	public boolean isPassable() {
		return bounds.getWidth() != 0 && bounds.getHeight() != 0;
	}
	
	public void setAngle(int angle) {
		this.angle = angle;
	}

	public void setColor(double red, double green, double blue, double alpha) {
		for (TextureBox textureBox : textureBoxes) {
			textureBox.setColor(red, green, blue, alpha);
		}
	}

	public void despawn() {
		if (spawnListener != null) {
			spawnListener.despawnEntity(this);
		} else {
			try {
				throw new NoListenerException();
			} catch (NoListenerException e) {
				e.printStackTrace();
			}
		}
	}

	public EntitySpawnListener getEntitySpawnListener() {
		return spawnListener;
	}

	public void setEntitySpawnListener(EntitySpawnListener spawnListener) {
		this.spawnListener = spawnListener;
	}

	@Override
	public String toString() {
		return String.format("%s:(%f|%f)", getClass().toString(), getBounds()
				.getX(), getBounds().getY());
	}
}
