package creatures;

import util.Bounds;
import entities.CreatureEntity;

public class Undead extends CreatureEntity {

	public Undead(int x, int y) {
		super(new Bounds(x, y, 20, 16, 0, 64), "undead");
	}

}
