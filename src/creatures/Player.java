package creatures;

import util.Bounds;
import entities.CreatureEntity;

public class Player extends CreatureEntity {
	private PlayerStatusListener listener;

	public Player(int x, int y) {
		super(new Bounds(x, y, 20, 16, 0, 64), "player");
	}
	
	@Override
	public void hurt(int damage, double directionX, double directionY) {
		listener.playerGotDamage(damage, directionX, directionY);
		super.hurt(damage, directionX, directionY);
	}

	public void setPlayerStatusListener(PlayerStatusListener listener) {
		this.listener = listener;
	}
}
