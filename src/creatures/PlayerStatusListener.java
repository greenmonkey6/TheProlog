package creatures;

public interface PlayerStatusListener {
	public void playerGotDamage(int damage, double directionX, double directionY);
}
