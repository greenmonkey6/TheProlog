package core;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

import game.Game;
import gui.GUI;
import static org.lwjgl.opengl.GL11.*;

/**
 * 
 * @author SenseLessNessGames
 * 
 * @version alpha 1.3.8
 *
 */
public class Main {
	public static final String name = "The Prolog - SenseLessNess Games";

	public static final int WIDTH = 1920;
	public static final int HEIGHT = 1080;

	private final double GAME_SPEED = 1;

	private int timer = 0;

	private boolean running = true;

	private long lastFrame;

	private GUI gui;
	private Game game;

	public Main() {
		init();

		while (running) {
			int delta = getDelta();

			render();
			update((int) (delta * GAME_SPEED));

			timer += delta;
			if (timer >= 250) {
				timer = 0;
				Display.setTitle(String.format("%s | %f fps @ %dms", name,
						1000d / delta, delta));
			}

			Display.update();
			Display.sync(60);

			if (Display.isCloseRequested()) {
				running = false;
			}
		}
		Display.destroy();
	}

	private void init() {
		try {
			Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
			Display.setTitle("Game");
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}

		glEnable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, WIDTH, HEIGHT, 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		glDisable(GL_LIGHTING);

		game = new Game();
		game.setRunMode(Game.BACKGROUND);
		gui = new GUI();
		
		game.setGameListener(gui);
		gui.setGUIListener(game);

		lastFrame = getTime();
	}

	private void render() {
		glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL11.GL_MODELVIEW);
		glLoadIdentity();
		glColor4d(1, 1, 1, 1);

		game.render();
		gui.render();
	}

	private void update(int delta) {
		game.update(delta);
		gui.update(delta);
	}

	private long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	private int getDelta() {
		long currentTime = getTime();
		int delta = (int) (currentTime - lastFrame);
		lastFrame = currentTime;
		return delta;
	}
	
	public static void exit() {
		Display.destroy();
		System.exit(0);
	}

	public static void main(String[] args) {
		new Main();
	}
}
