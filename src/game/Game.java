package game;

import java.util.ArrayList;

import org.lwjgl.opengl.Display;

import util.AnimatedTextureBox;
import util.Bounds;
import util.EntitySpawnListener;
import creatures.Player;
import creatures.PlayerStatusListener;
import creatures.Undead;
import entities.AttackEntity;
import entities.CreatureEntity;
import entities.Entity;
import entities.MoveableEntity;
import entities.UnmoveableEntity;
import gui.GUIListener;
import guiComponents.Layer;

public class Game extends Layer implements GUIListener, EntitySpawnListener,
		PlayerStatusListener {
	public static final int REGULAR = 0x0;
	public static final int PAUSED = 0x1;
	public static final int BACKGROUND = 0x2;

	private int runMode;

	private GameListener listener;

	private double playerCameraX, playerCameraY;
	private double relativeCameraX, relativeCameraY;
	private int zoom;

	private int damageAnimCounter;
	private int damageAnimTime;
	private double damageDirectionX, damageDirectionY;

	private ArrayList<Entity> spawnEntities;
	private ArrayList<Boolean> spawnEntitiesType;

	private ArrayList<Entity> entities;
	private ArrayList<MoveableEntity> moveableEntities;
	private ArrayList<AttackEntity> attackEntities;
	private ArrayList<CreatureEntity> creatureEntities;

	private Player player;
	private Entity map;

	public Game() {
		player = new Player(840, 550);
		player.setEntitySpawnListener(this);
		player.setPlayerStatusListener(this);
		player.setEntitySpawnListener(this);

		playerCameraX = 0;
		playerCameraY = 0;
		relativeCameraX = 0;
		relativeCameraY = 0;
		zoom = 4;

		damageAnimCounter = 0;
		damageAnimTime = 100;

		spawnEntities = new ArrayList<Entity>();
		spawnEntitiesType = new ArrayList<Boolean>();

		entities = new ArrayList<Entity>();
		moveableEntities = new ArrayList<MoveableEntity>();
		attackEntities = new ArrayList<AttackEntity>();
		creatureEntities = new ArrayList<CreatureEntity>();

		spawnEntity(this.player);

		map = new UnmoveableEntity(new Bounds(0, 0, 0, 0, 0, 0),
				new AnimatedTextureBox("map", 0, 0, 1, 1));
		spawnEntity(map);

		spawnEntity(new UnmoveableEntity(new Bounds(759, 499, 193, 16, 0, 96)));
		spawnEntity(new UnmoveableEntity(new Bounds(743, 515, 16, 104, 0, 96)));
		spawnEntity(new UnmoveableEntity(new Bounds(952, 515, 16, 255, 0, 96)));
		spawnEntity(new UnmoveableEntity(new Bounds(750, 619, 7, 46, 0, 96)));
		spawnEntity(new UnmoveableEntity(new Bounds(743, 666, 16, 104, 0, 96)));
		spawnEntity(new UnmoveableEntity(new Bounds(759, 770, 193, 16, 0, 96)));

		spawnEntity(new UnmoveableEntity(new Bounds(638, 587, 105, 16, 0, 96)));
		spawnEntity(new UnmoveableEntity(new Bounds(487, 587, 105, 16, 0, 96)));
		spawnEntity(new UnmoveableEntity(new Bounds(638, 682, 105, 16, 0, 96)));
		spawnEntity(new UnmoveableEntity(new Bounds(487, 682, 105, 16, 0, 96)));

		Undead undead = new Undead(605, 600);
		spawnEntity(undead);

		Undead undead2 = new Undead(770, 600);
		undead2.setKiMode(CreatureEntity.KI_ATTACKING_PLAYER);
		spawnEntity(undead2);

		// AnimatedTextureBox texture = new AnimatedTextureBox(
		// "fireballMeteorArtSpell", 0, -16, 16, 1);
		// texture.setAnim(0, 0, 10, AnimatedTextureBox.HORIZONTAL, 50);
		// AttackEntity attackEntity = new AttackEntity(new Bounds(890, 550, 16,
		// 16, 0, 96), 50, texture);
		// attackEntity.setDirection(1, 0.7);
		// attackEntity.setSpeed(0.05);
		// spawnEntity(attackEntity);
	}

	@Override
	public void render() {
		if (runMode == REGULAR || runMode == PAUSED) {
			for (Entity entity : entities) {
				entity.render(playerCameraX + relativeCameraX, playerCameraY
						+ relativeCameraY, zoom);
			}
		}
	}

	@Override
	public void update(int delta) {
		if (runMode == REGULAR) {
			if (damageAnimCounter > 0) {
				damageAnimCounter -= delta;

				double anim = 1 - 1d * damageAnimCounter / damageAnimTime;

				// double function = -3.2 * anim * anim * anim + 0.8 * anim *
				// anim
				// + 2.4 * anim; // f(x) = -3.2x�+0.8x�+2.4x
				double function = -41.53 * anim * anim * anim * anim + 88.4
						* anim * anim * anim - 60.57 * anim * anim + 13.7
						* anim;
				// f(x) = -41.53x^4 + 88.4x� - 60.57x� + 13.7x
				relativeCameraX = damageDirectionX * function * 10;
				relativeCameraY = damageDirectionY * function * 10;
			} else {
				damageAnimCounter = 0;

				relativeCameraX = 0;
				relativeCameraY = 0;
			}

			// Spawning
			for (int i = 0; i < spawnEntities.size(); i++) {
				Entity entity = spawnEntities.get(i);
				if (spawnEntitiesType.get(i).booleanValue()) {
					entities.add(entity);
					entity.setEntitySpawnListener(this);

					if (entity instanceof MoveableEntity) {
						moveableEntities.add((MoveableEntity) entity);
					}

					if (entity instanceof AttackEntity) {
						attackEntities.add((AttackEntity) entity);
					}

					if (entity instanceof CreatureEntity) {
						creatureEntities.add((CreatureEntity) entity);
					}
				} else {
					entities.remove(entity);
					moveableEntities.remove(entity);
					attackEntities.remove(entity);
					creatureEntities.remove(entity);
				}
			}
			spawnEntities.clear();
			spawnEntitiesType.clear();

			// Updaten
			for (Entity entity : entities) {
				entity.update(delta);
			}

			// Kollidieren
			for (MoveableEntity moveableEntity : moveableEntities) {
				for (Entity entity : entities) {
					moveableEntity.collide(entity);
				}
			}

			// KI
			for (CreatureEntity creatureEntity : creatureEntities) {
				creatureEntity.ki(entities);
			}

			// Sortieren
			ArrayList<Entity> temp = new ArrayList<Entity>();
			for (Entity entity : entities) {
				if (!(entity instanceof MoveableEntity)) {
					temp.add(entity);
				}
			}

			for (MoveableEntity moveableEntity : moveableEntities) {
				int pos = temp.size();

				for (int i = 0; i < temp.size(); i++) {
					if (moveableEntity.getBounds().getY() < temp.get(i)
							.getBounds().getY()
							&& temp.get(i) != map) {
						pos = i;
						break;
					}
				}

				temp.add(pos, moveableEntity);
			}

			entities = temp;

			// Kamera bewegen
			playerCameraX = player.getMidX() - (Display.getWidth() / zoom) / 2;
			playerCameraY = player.getMidY() - (Display.getHeight() / zoom) / 2;

			listener.handlePlayerStats(player.getLife());
			listener.handleInventory();
		}
	}

	@Override
	public void scale() {

	}

	public void setGameListener(GameListener listener) {
		this.listener = listener;
	}

	public void setRunMode(int runMode) {
		this.runMode = runMode;
	}

	public int getRunMode() {
		return runMode;
	}

	@Override
	public void handleAttack(int attack) {
		switch (attack) {
		case GUIListener.PEACEFUL:
			// XXX M�sste hier eigentlich stehen (CreatureEntity darf Angriff
			// nicht sofort abbrechen):
			// player.attack(CreatureEntity.PEACEFUL);
			break;
		case GUIListener.PUNCH:
			// player.attack(CreatureEntity.PUNCH);
			break;
		case GUIListener.SPELL1:
			player.attack(CreatureEntity.FIRE_BALL);
			break;
		case GUIListener.SPELL2:

			break;
		case GUIListener.SPELL3:

			break;
		case GUIListener.SPELL4:

			break;
		case GUIListener.SPELL5:

			break;
		case GUIListener.SPELL6:

			break;
		}
	}

	@Override
	public void handleWalkCommand(double directionX, double directionY,
			int walkMode) {
		if (walkMode != GUIListener.STANDING) {
			player.setDirectionAsUnitVector(directionX, directionY);

			switch (walkMode) {
			case GUIListener.WALKING:
				player.setWalkMode(CreatureEntity.WALKING);
				break;

			case GUIListener.RUNNING:
				player.setWalkMode(CreatureEntity.RUNNING);
				break;
			}
		} else {
			player.setWalkMode(CreatureEntity.STANDING);
		}
	}

	@Override
	public void despawnEntity(Entity entity) {
		spawnEntities.add(entity);
		spawnEntitiesType.add(false);
	}

	@Override
	public void spawnEntity(Entity entity) {
		spawnEntities.add(entity);
		spawnEntitiesType.add(true);
	}

	@Override
	public void playerGotDamage(int damage, double directionX, double directionY) {
		listener.startDamageAnim();

		damageAnimCounter = damageAnimTime;
		damageDirectionX = directionX;
		damageDirectionY = directionY;
	}

	@Override
	public void handleRunMode(int runMode) {
		switch (runMode) {
		case GUIListener.REGULAR:
			setRunMode(REGULAR);
			break;
		case GUIListener.PAUSED:
			setRunMode(PAUSED);
			break;
		case GUIListener.BACKGROUND:
			setRunMode(BACKGROUND);
			break;
		}
	}
}
