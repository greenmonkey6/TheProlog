package game;

public interface GameListener {
	public void handlePlayerStats(int life);

	public void handleInventory();
	
	public void startDamageAnim();
}
