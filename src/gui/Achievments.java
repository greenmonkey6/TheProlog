package gui;

import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glRecti;

import org.lwjgl.opengl.Display;

import guiComponents.Layer;
import guiComponents.MenuLayer;
import guiComponents.RelativeLayer;

public class Achievments extends Layer {
	private MainMenu mainMenu;

	private MenuLayer menu;
	private RelativeLayer layer;

	public Achievments() {
		layer = new RelativeLayer(100, 100);

		menu = new MenuLayer();
		menu.setBounds(30, 35, 70, 95);
		menu.setButtons("ZUR�CK");

		layer.addComponent(menu);

		addComponent(layer);
	}

	@Override
	public void update(int delta) {
		switch (menu.getCurrentButton()) {
		case 0:
			setNextLayer(mainMenu);
			break;
		}
		
		super.update(delta);
	}

	@Override
	public void render() {
		glColor4f(1, 1, 1, 1);
		glRecti(0, 0, Display.getWidth(), Display.getHeight());

		super.render();
	}

	@Override
	public void scale() {
		layer.setBounds(0, 0, getWidth(), getHeight());
	}

	public void setMainMenu(MainMenu mainMenu) {
		this.mainMenu = mainMenu;
	}
}
