package gui;

import org.lwjgl.opengl.Display;

import core.Main;
import guiComponents.Layer;
import guiComponents.MenuLayer;
import guiComponents.RelativeLayer;
import static org.lwjgl.opengl.GL11.*;

public class MainMenu extends Layer {
	private InGame inGame;
	private Options options;
	private Achievments achievments;
	private Credits credits;

	private MenuLayer menu;
	private RelativeLayer layer;

	public MainMenu() {
		layer = new RelativeLayer(100, 100);

		menu = new MenuLayer();
		menu.setBounds(30, 35, 70, 95);
		menu.setButtons("STORY MODUS", "ARCADE MODUS", "OPTIONEN",
				"ACHIEVMENTS", "CREDITS", "SPIEL BEENDEN");

		layer.addComponent(menu);

		addComponent(layer);
	}

	@Override
	public void scale() {
		layer.setBounds(0, 0, getWidth(), getHeight());
	}

	@Override
	public void render() {
		glDisable(GL_TEXTURE_2D);
		glColor4f(1, 1, 1, 1);
		glRecti(0, 0, Display.getWidth(), Display.getHeight());

		super.render();
	}

	@Override
	public void update(int delta) {
		switch (menu.getCurrentButton()) {
		case 0:
			setNextLayer(inGame);
			break;
		case 1:
			break;
		case 2:
			setNextLayer(options);
			break;
		case 3:
			setNextLayer(achievments);
			break;
		case 4:
			setNextLayer(credits);
			break;
		case 5:
			Main.exit();
			break;
		}

		super.update(delta);
	}

	public void setInGame(InGame inGame) {
		this.inGame = inGame;
	}
	
	public void setOptions(Options options) {
		this.options = options;
	}
	
	public void setAchievments(Achievments achievments) {
		this.achievments = achievments;
	}
	
	public void setCredits(Credits credits) {
		this.credits = credits;
	}
}
