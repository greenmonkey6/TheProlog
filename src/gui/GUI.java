package gui;

import org.lwjgl.opengl.Display;

import game.GameListener;
import guiComponents.Component;
import guiComponents.Layer;

public class GUI extends Layer implements GameListener {
	protected static GUIListener listener;

	InGame inGame;
	Options options;
	Achievments achievments;
	Credits credits;
	private Layer currentLayer;

	public GUI() {
		inGame = new InGame();

		MainMenu mainMenu = new MainMenu();
		mainMenu.setBounds(0, 0, Display.getWidth(), Display.getHeight());

		options = new Options();
		options.setBounds(0, 0, Display.getWidth(), Display.getHeight());

		achievments = new Achievments();
		achievments.setBounds(0, 0, Display.getWidth(), Display.getHeight());

		credits = new Credits();
		credits.setBounds(0, 0, Display.getWidth(), Display.getHeight());

		inGame.setMainMenu(mainMenu);
		mainMenu.setInGame(inGame);
		mainMenu.setOptions(options);
		mainMenu.setAchievments(achievments);
		mainMenu.setCredits(credits);
		options.setMainMenu(mainMenu);
		achievments.setMainMenu(mainMenu);
		credits.setMainMenu(mainMenu);

		currentLayer = mainMenu;
		addComponent(currentLayer);
	}

	@Override
	public void update(int delta) {
		Layer temp = currentLayer.getNextLayer();
		if (temp != null) {
			removeComponent(currentLayer);
			currentLayer = temp;
			addComponent(currentLayer);
		}
		super.update(delta);
	}

	@Override
	public void scale() {

	}

	public void setGUIListener(GUIListener listener) {
		this.listener = listener;
	}

	@Override
	public void handlePlayerStats(int life) {
		inGame.setValues(life);
	}

	@Override
	public void handleInventory() {

	}

	@Override
	public void startDamageAnim() {
		inGame.startDamageAnim();
	}
}
