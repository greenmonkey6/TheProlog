package gui;

import static org.lwjgl.opengl.GL11.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import entities.Entity;
import guiComponents.Layer;

public class InGame extends Layer {
	private static final double SQRT05 = Math.sqrt(0.5);
	
	private MainMenu mainMenu;

	private Texture lifeBar;
	private Texture monatlicheBlutungenOderSo;
	private Texture wheel;

	private int life;
	private int damageAnimCounter;
	private int damageAnimTime;

	private boolean isWheel;
	private int wheelPosX, wheelPosY;
	private int wheelSelection;

	public InGame() {
		try {
			lifeBar = TextureLoader.getTexture("PNG", new FileInputStream(
					new File("res/GUI/lifeBar.png")));
			monatlicheBlutungenOderSo = TextureLoader.getTexture("PNG",
					new FileInputStream(new File(
							"res/GUI/monatlicheBlutungenOderSo.png")));
			wheel = TextureLoader.getTexture("PNG", new FileInputStream(
					new File("res/GUI/wheel.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		life = 0;

		damageAnimCounter = 0;
		damageAnimTime = 1000;

		isWheel = false;
		wheelPosX = 0;
		wheelPosY = 0;
		wheelSelection = 0;
	}

	@Override
	public void render() {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		if (damageAnimCounter != 0) {
			double anim = 1d * damageAnimCounter / damageAnimTime;

			GL11.glColor4d(1, 1, 1, Math.sin(anim * 0.5 * Math.PI));
			monatlicheBlutungenOderSo.bind();
			glLoadIdentity();
			glTranslatef(0, 0, 0);
			glBegin(GL_QUADS);
			glTexCoord2f(0, 0);
			glVertex2i(0, 0);
			glTexCoord2f(1, 0);
			glVertex2i(monatlicheBlutungenOderSo.getImageWidth(), 0);
			glTexCoord2f(1, 1);
			glVertex2i(monatlicheBlutungenOderSo.getImageWidth(),
					monatlicheBlutungenOderSo.getImageHeight());
			glTexCoord2f(0, 1);
			glVertex2i(0, monatlicheBlutungenOderSo.getImageHeight());
			glEnd();
			glLoadIdentity();
		}

		glColor4d(1, 1, 1, 0.5);
		lifeBar.bind();

		for (int i = 0; i < life / 10; i++) {
			int zoom = 4;

			int x = (int) (Display.getWidth() * 0.5 + (lifeBar
					.getTextureWidth() * zoom + zoom)
					* (i - 5));
			int y = (int) (Display.getHeight() * 0.9);

			glLoadIdentity();
			glTranslatef(x, y, 0);
			glBegin(GL_QUADS);
			glTexCoord2f(0, 0);
			glVertex2i(0, 0);
			glTexCoord2f(1, 0);
			glVertex2i(lifeBar.getImageWidth() * zoom, 0);
			glTexCoord2f(1, 1);
			glVertex2i(lifeBar.getImageWidth() * zoom, lifeBar.getImageHeight()
					* zoom);
			glTexCoord2f(0, 1);
			glVertex2i(0, lifeBar.getImageHeight() * zoom);
			glEnd();
			glLoadIdentity();
		}

		if (isWheel) {
			glColor4d(1, 1, 1, 1);
			wheel.bind();
			glLoadIdentity();
			glTranslatef(wheelPosX - wheel.getImageWidth() / 16, wheelPosY
					- wheel.getImageHeight() / 2, 0);
			glBegin(GL_QUADS);
			glTexCoord2f(wheelSelection / 8f, 0);
			glVertex2i(0, 0);
			glTexCoord2f((wheelSelection + 1) / 8f, 0);
			glVertex2i(wheel.getImageWidth() / 8, 0);
			glTexCoord2f((wheelSelection + 1) / 8f, 1);
			glVertex2i(wheel.getImageWidth() / 8, wheel.getImageHeight());
			glTexCoord2f(wheelSelection / 8f, 1);
			glVertex2i(0, wheel.getImageHeight());
			glEnd();
			glLoadIdentity();
		}

		super.render();
	}

	@Override
	public void update(int delta) {
		GUI.listener.handleRunMode(GUIListener.REGULAR);
		
		int walkMode;
		double directionX = 0;
		double directionY = 0;

		if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			walkMode = GUIListener.RUNNING;
		} else {
			walkMode = GUIListener.WALKING;
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
			if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
				directionX = -SQRT05;
				directionY = -SQRT05;
			} else if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
				directionX = SQRT05;
				directionY = -SQRT05;
			} else {
				directionX = 0;
				directionY = -1;
			}
		} else if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
			if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
				directionX = -SQRT05;
				directionY = SQRT05;
			} else if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
				directionX = SQRT05;
				directionY = SQRT05;
			} else {
				directionX = 0;
				directionY = 1;
			}
		} else if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
			directionX = -1;
			directionY = 0;
		} else if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
			directionX = 1;
			directionY = 0;
		} else {
			walkMode = GUIListener.STANDING;
		}

		GUI.listener.handleWalkCommand(directionX, directionY, walkMode);

		while (Keyboard.next()) {
			if (!Keyboard.getEventKeyState()) {
				if (Keyboard.getEventKey() == Keyboard.KEY_F3) {
					Entity.showBounds = !Entity.showBounds;
				}
				
				if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
					setNextLayer(mainMenu);
					GUI.listener.handleRunMode(GUIListener.BACKGROUND);
				}
			}
		}

		int attack = GUIListener.PEACEFUL;

		while (Mouse.next()) {
			if (Mouse.getEventButton() == 0) {
				attack = GUIListener.PUNCH;
			} else if (Mouse.getEventButton() == 1) {
				if (Mouse.getEventButtonState()) {
					isWheel = true;
					wheelPosX = Mouse.getX();
					wheelPosY = Display.getHeight() - Mouse.getY() - 1;
				} else {
					isWheel = false;

					switch (wheelSelection) {
					case 1:
						attack = GUIListener.SPELL1;
						break;
					case 2:
						attack = GUIListener.SPELL2;
						break;
					case 3:
						attack = GUIListener.SPELL3;
						break;
					case 4:
						attack = GUIListener.SPELL4;
						break;
					case 5:
						attack = GUIListener.SPELL5;
						break;
					case 6:
						attack = GUIListener.SPELL6;
						break;
					case 0:
						// nothing
						break;
					}
				}
			}
		}

		if (Mouse.isButtonDown(1)) {
			int vecX = Mouse.getX() - wheelPosX;
			int vecY = Display.getHeight() - Mouse.getY() - 1 - wheelPosY;
			double length = Math.sqrt(vecX * vecX + vecY * vecY);
			double angle = Math.acos(vecX / length) * 180 / Math.PI;

			if (length > 10) {
				if (vecY < 0) {
					if (angle < 60) {
						wheelSelection = 1;
					} else if (angle < 120) {
						wheelSelection = 2;
					} else {
						wheelSelection = 3;
					}
				} else {
					if (angle < 60) {
						wheelSelection = 6;
					} else if (angle < 120) {
						wheelSelection = 5;
					} else {
						wheelSelection = 4;
					}
				}
			} else {
				wheelSelection = 0;
			}
		}

		if (damageAnimCounter > 0) {
			damageAnimCounter -= delta;
		} else {
			damageAnimCounter = 0;
		}

		GUI.listener.handleAttack(attack);

		super.update(delta);
	}

	@Override
	public void scale() {

	}

	public void setValues(int life) {
		this.life = life;
	}

	public void startDamageAnim() {
		damageAnimCounter = damageAnimTime;
	}
	
	public void setMainMenu(MainMenu mainMenu) {
		this.mainMenu = mainMenu;
	}
}
