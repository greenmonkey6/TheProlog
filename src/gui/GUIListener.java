package gui;

public interface GUIListener {
	public static final int PEACEFUL = 0x0;
	public static final int PUNCH = 0x1;
	public static final int SPELL1 = 0x2;
	public static final int SPELL2 = 0x3;
	public static final int SPELL3 = 0x4;
	public static final int SPELL4 = 0x5;
	public static final int SPELL5 = 0x6;
	public static final int SPELL6 = 0x7;

	public static final int STANDING = 0x10;
	public static final int WALKING = 0x11;
	public static final int RUNNING = 0x12;

	public static final int REGULAR = 0x20;
	public static final int PAUSED = 0x21;
	public static final int BACKGROUND = 0x22;

	public void handleAttack(int attack);

	public void handleWalkCommand(double directionX, double directionY,
			int walkMode);

	public void handleRunMode(int runMode);
}
