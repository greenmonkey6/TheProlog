package util;

public class AnimatedTextureBox extends TextureBox {
	public static final int NO_ANIMATION = 0x0;
	public static final int HORIZONTAL = 0x1;
	public static final int VERTICAL = 0x2;

	private int animTimer;
	private int animDuration;

	private int currentAnim;

	private int animX;
	private int animY;
	private int animLength;
	private int animDirection;

	private AnimatedTextureBox synchronizedTextureBox;

	private boolean done;

	public AnimatedTextureBox(String textureName, int x, int y, int columns,
			int rows) {
		super(textureName, x, y, columns, rows);

		init();
	}

	private void init() {
		animTimer = 0;
		animDuration = 0;

		currentAnim = 0;

		animX = 0;
		animY = 0;
		animLength = 0;
		animDirection = NO_ANIMATION;

		done = false;
	}

	@Override
	public void update(int delta) {
		if (animDirection != NO_ANIMATION) {
			animTimer += delta;

			if (animTimer >= animDuration) {
				int value = animTimer / animDuration;
				currentAnim += value;
				animTimer -= value * animDuration;
			}

			if (currentAnim >= animLength) {
				currentAnim = currentAnim % animLength;
				done = true;
			}
		} else {
			animTimer = 0;
			currentAnim = 0;
		}

		int x, y;

		switch (animDirection) {
		case HORIZONTAL:
			x = animX + currentAnim;
			y = animY;
			break;
		case VERTICAL:
			x = animX;
			y = animY + currentAnim;
			break;
		case NO_ANIMATION:
			x = animX;
			y = animY;
			break;
		default:
			x = animX;
			y = animY;
			break;
		}
		setCurrentTexture(x, y);

		super.update(delta);
	}

	public void setAnim(int x, int y, int length, int direction, int duration) {
		if (x != animX || y != animY || length != animLength
				|| direction != animDirection || duration != animDuration) {
			animX = x;
			animY = y;
			animLength = length;
			animDirection = direction;
			animDuration = duration;

			done = false;

			if (currentAnim >= animLength) {
				currentAnim = animLength - 1;
			}
		}
	}

	public int getCurrentAnim() {
		return currentAnim;
	}

	public int getAnimTimer() {
		return animTimer;
	}

	public void setSynchronizedTextureBox(AnimatedTextureBox textureBox) {
		synchronizedTextureBox = textureBox;
	}

	public void sync() {
		if (synchronizedTextureBox != null) {
			currentAnim = synchronizedTextureBox.getCurrentAnim();
			animTimer = synchronizedTextureBox.getAnimTimer();
		}
	}

	public void resetAnim() {
		currentAnim = 0;
	}

	public boolean isDone() {
		return done;
	}
}
