package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import entities.CreatureEntity;

public class FileAnimatedTextureBox extends AnimatedTextureBox {
	private int[][][] animations;

	public FileAnimatedTextureBox(String creatureName, String textureName) {
		super("creatures/" + creatureName + "/" + textureName, 0, 0, 1, 1);

		BufferedReader reader;
		int largest = 0;
		try {
			reader = new BufferedReader(new FileReader(new File(
					"res/creatures/" + creatureName + "/anims.txt")));
			reader.readLine();
			for (String currentLine = reader.readLine(); currentLine != null; currentLine = reader
					.readLine()) {
				int temp = Integer.parseInt(new StringTokenizer(currentLine,
						" ", false).nextToken());
				if (temp > largest) {
					largest = temp;
				}
			}

			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		animations = new int[largest + 1][CreatureEntity.ORIENTATIONS][5];

		try {
			reader = new BufferedReader(new FileReader(new File(
					"res/creatures/" + creatureName + "/" + textureName
							+ ".txt")));

			StringTokenizer tok = new StringTokenizer(reader.readLine(), " ",
					false);

			setDrawX(Integer.parseInt(tok.nextToken()));
			setDrawY(Integer.parseInt(tok.nextToken()));
			setColumns(Integer.parseInt(tok.nextToken()));
			setRows(Integer.parseInt(tok.nextToken()));

			for (String currentLine = reader.readLine(); currentLine != null; currentLine = reader
					.readLine()) {
				int anim, orientation;

				tok = new StringTokenizer(currentLine, " ", false);

				anim = Integer.parseInt(tok.nextToken());
				String temp = tok.nextToken();
				if (temp.equals("N")) {
					orientation = CreatureEntity.NORTH;
				} else if (temp.equals("NE")) {
					orientation = CreatureEntity.NORTH_EAST;
				} else if (temp.equals("E")) {
					orientation = CreatureEntity.EAST;
				} else if (temp.equals("SE")) {
					orientation = CreatureEntity.SOUTH_EAST;
				} else if (temp.equals("S")) {
					orientation = CreatureEntity.SOUTH;
				} else if (temp.equals("SW")) {
					orientation = CreatureEntity.SOUTH_WEST;
				} else if (temp.equals("W")) {
					orientation = CreatureEntity.WEST;
				} else if (temp.equals("NW")) {
					orientation = CreatureEntity.NORTH_WEST;
				} else {
					orientation = -1;
				}

				for (int i = 0; tok.hasMoreTokens(); i++) {
					String currentToken = tok.nextToken();
					if (currentToken.equals("h")) {
						animations[anim][orientation][i] = AnimatedTextureBox.HORIZONTAL;
					} else if (currentToken.equals("n")) {
						animations[anim][orientation][i] = AnimatedTextureBox.NO_ANIMATION;
					} else if (currentToken.equals("v")) {
						animations[anim][orientation][i] = AnimatedTextureBox.VERTICAL;
					} else {
						animations[anim][orientation][i] = Integer
								.parseInt(currentToken);
					}
				}
			}

			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setAnim(int value, int orientation) {
		super.setAnim(animations[value][orientation][0],
				animations[value][orientation][1],
				animations[value][orientation][2],
				animations[value][orientation][3],
				animations[value][orientation][4]);
	}
}
