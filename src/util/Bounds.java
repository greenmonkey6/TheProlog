package util;

import static org.lwjgl.opengl.GL11.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

public class Bounds {
	private double x, y;
	private int width, height;
	private int z, depth;
	private Texture texture;

	public Bounds(double x, double y, int width, int height, int z, int depth) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.z = z;
		this.depth = depth;

		try {
			texture = TextureLoader.getTexture("PNG", new FileInputStream(
					new File("res/bounds.png")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void render(double cameraX, double cameraY, int zoom) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		glColor4d(1, 1, 1, 0.1);

		texture.bind();
		glLoadIdentity();
		glTranslatef(((float) (x - cameraX) * zoom),
				((float) (y - cameraY) * zoom), 0);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2i(0, 0);
		glTexCoord2f(1, 0);
		glVertex2i(width * zoom, 0);
		glTexCoord2f(1, 1);
		glVertex2i(width * zoom, height * zoom);
		glTexCoord2f(0, 1);
		glVertex2i(0, height * zoom);
		glEnd();
		glLoadIdentity();
	}

	public void setX(double x) {
		this.x = x;
	}

	public void addX(double x) {
		this.x += x;
	}

	public double getX() {
		return x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void addY(double y) {
		this.y += y;
	}

	public double getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getZ() {
		return z;
	}

	public int getDepth() {
		return depth;
	}

	public boolean intersects(Bounds bounds) {
		if (bounds.getX() + bounds.getWidth() > getX()
				&& bounds.getX() < getX() + getWidth()
				&& bounds.getY() + bounds.getHeight() > getY()
				&& bounds.getY() < getY() + getHeight()
				&& bounds.getZ() + bounds.getDepth() > getZ()
				&& bounds.getZ() < getZ() + getDepth()) {
			return true;
		} else {
			return false;
		}
	}
}
