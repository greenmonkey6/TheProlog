package util;

import static org.lwjgl.opengl.GL11.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

public class TextureBox {
	private Texture texture;
	private String textureName;
	private double posX, posY;
	private int width, height;
	private int drawX, drawY;
	private int columns, rows;
	private int angle = 0;

	private int currentX, currentY;

	private double red, green, blue, alpha;

	public TextureBox(String textureName, int x, int y, int columns, int rows) {
		this.textureName = textureName;

		posX = 0;
		posY = 0;

		drawX = x;
		drawY = y;

		this.columns = columns;
		this.rows = rows;

		if (textureName != null && !textureName.equals("")) {
			try {
				texture = TextureLoader.getTexture("PNG", new FileInputStream(
						new File("res/" + textureName + ".png")));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			width = texture.getImageWidth() / this.columns;
			height = texture.getImageHeight() / this.rows;
		} else {
			throw new IllegalArgumentException();
		}

		currentX = 0;
		currentY = 0;

		red = 1;
		green = 1;
		blue = 1;
		alpha = 1;
	}

	public void render(double cameraX, double cameraY, int zoom) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		glColor4d(red, green, blue, alpha);

		float x1 = 1f / columns * currentX;
		float x2 = 1f / columns * (currentX + 1);
		float y1 = 1f / rows * currentY;
		float y2 = 1f / rows * (currentY + 1);

		texture.bind();
		glLoadIdentity();
		
		glTranslatef(((float) (posX + drawX - cameraX) * zoom), ((float) (posY
				+ drawY - cameraY) * zoom), 0);
		glRotatef(angle, 0, 0, 1);

		glBegin(GL_QUADS);
		glTexCoord2f(x1, y1);
		glVertex2i(0, 0);
		glTexCoord2f(x2, y1);
		glVertex2i(width * zoom, 0);
		glTexCoord2f(x2, y2);
		glVertex2i(width * zoom, height * zoom);
		glTexCoord2f(x1, y2);
		glVertex2i(0, height * zoom);
		glEnd();
		glLoadIdentity();
	}

	public void update(int delta) {

	}

	public void setCurrentTexture(int x, int y) {
		currentX = x;
		currentY = y;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosX() {
		return posX;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public double getPosY() {
		return posY;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getDrawX() {
		return drawX;
	}

	public int getDrawY() {
		return drawY;
	}

	public void setColor(double red, double green, double blue, double alpha) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}

	public void setColumns(int columns) {
		this.columns = columns;
		width = texture.getImageWidth() / this.columns;
	}

	public int getColumns() {
		return columns;
	}

	public void setRows(int rows) {
		this.rows = rows;
		height = texture.getImageHeight() / this.rows;
	}

	public int getRows() {
		return rows;
	}

	public void setDrawX(int drawX) {
		this.drawX = drawX;
	}

	public void setDrawY(int drawY) {
		this.drawY = drawY;
	}
	
	public void setAngle(int angle) {
		this.angle = angle;
	}
	
	public int getAngle() {
		return angle;
	}

	@Override
	public String toString() {
		return textureName;
	}
}
