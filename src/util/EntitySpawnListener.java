package util;

import entities.Entity;

public interface EntitySpawnListener {
	public void spawnEntity(Entity entity);
	public void despawnEntity(Entity entity);
}
