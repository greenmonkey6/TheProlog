package util;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Util {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object getInstance(String str) {
		if (str.contains("(")) {
			// String in Parameter und Klassenname aufteilen
			int breaking = 0;
			for (int i = 0; i < str.length(); i++) {
				if (str.charAt(i) == '(') {
					breaking = i;
					break;
				}
			}
			String typeName = str.substring(0, breaking);
			String parameterStr = str.substring(breaking + 1, str.length() - 1);

			// Klasse des Objekts erstellen
			Class type;
			try {
				type = Class.forName(typeName);
			} catch (ClassNotFoundException e) {
				type = null;
				e.printStackTrace();
			}

			// Listen der Parameter
			ArrayList<Object> parameters = new ArrayList<Object>();
			ArrayList<Class> parameterTypes = new ArrayList<Class>();

			// Tokenizer erstellen, der die Parameter trennt
			StringTokenizer tok = new StringTokenizer(parameterStr, ",", false);

			// ParameterString durchgehen
			while (tok.hasMoreTokens()) {
				// Aktuellen Parameter anfordern
				String currentParameter = "";

				int bracketDif;
				do {
					currentParameter += tok.nextToken();

					bracketDif = 0;
					for (int i = 0; i < currentParameter.length(); i++) {
						if (currentParameter.charAt(i) == '(') {
							bracketDif++;
						} else if (currentParameter.charAt(i) == ')') {
							bracketDif--;
						}
					}

					if (bracketDif != 0) {
						currentParameter += ",";
					}
				} while (bracketDif != 0);

				// Instanz von aktuellem Paramter erstellen
				Object obj = getInstance(currentParameter);

				// Parameter den Listen hinzuf�gen
				parameters.add(obj);
				if (obj instanceof Integer) {
					parameterTypes.add(int.class);
				} else if (obj instanceof Double) {
					parameterTypes.add(double.class);
				} else {
					parameterTypes.add(obj.getClass());
				}
			}

			Object object;
			if (!type.isArray()) {
				// Parameterklassenliste in Array umwandeln
				Class[] parameterTypesArray = new Class[parameterTypes.size()];
				for (int i = 0; i < parameterTypes.size(); i++) {
					parameterTypesArray[i] = parameterTypes.get(i);
				}

				// Konstruktor mit Parameter-Klassen erstellen
				Constructor constructor;
				try {
					constructor = type.getConstructor(parameterTypesArray);
				} catch (NoSuchMethodException e) {
					constructor = null;
					e.printStackTrace();
				} catch (SecurityException e) {
					constructor = null;
					e.printStackTrace();
				}

				// Parameterobjektliste in Array umwandeln
				Object[] parametersArray = new Object[parameters.size()];
				for (int i = 0; i < parameters.size(); i++) {
					parametersArray[i] = parameters.get(i);
				}

				// Instanz aus Konstruktor mit Parametern erstellen
				try {
					object = constructor.newInstance(parametersArray);
				} catch (InstantiationException e) {
					object = null;
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					object = null;
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					object = null;
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					object = null;
					e.printStackTrace();
				}
			} else {
				// Array erstellen
				object = Array.newInstance(type.getComponentType(),
						parameters.size());
				
				// Parameter in das Array einf�gen
				for (int i = 0; i < parameters.size(); i++) {
					Array.set(object, i, parameters.get(i));
				}
			}

			return object;
		} else {
			if (str.contains("\"")) {
				return str.substring(1, str.length() - 1);
			} else if (str.contains(".")) {
				return Double.parseDouble(str);
			} else {
				return Integer.parseInt(str);
			}
		}
	}
}
