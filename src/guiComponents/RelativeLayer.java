package guiComponents;

/**
 * 
 * @author Tobias Schleifstein
 *
 */
public class RelativeLayer extends Layer {
	/**
	 * Relative Breite, von welcher ausgegangen wird, um die Components zu
	 * skalieren.
	 */
	public final int RELATIVE_WIDTH;

	/**
	 * Relative H�he, von welcher ausgegangen wird, um die Components zu
	 * skalieren.
	 */
	public final int RELATIVE_HEIGHT;

	/** Absolute Position der Components auf dem Bildschirm. */
	private Rectangle[] absoluteBounds;

	/**
	 * Erstellt eine RelativeLayer mit einer bestimmten relativen Aufl�sung.
	 * 
	 * @param relativeWidth
	 *            Relative Breite des Layers.
	 * @param relativeHeight
	 *            Relative H�he des Layers.
	 */
	public RelativeLayer(int relativeWidth, int relativeHeight) {
		// Relative Aufl�sung speichern
		this.RELATIVE_WIDTH = relativeWidth;
		this.RELATIVE_HEIGHT = relativeHeight;
	}

	/**
	 * Setzt die absolute Gr��e der Components ein, aktualisiert diese und setzt
	 * anschlie�end wieder die originale Gr��e ein.
	 */
	@Override
	public void update(int delta) {
		Component[] components = getComponents();
		Rectangle[] originalBounds = new Rectangle[components.length];

		// Originale Position speichern und absolute Position einsetzen
		for (int i = 0; i < components.length; i++) {
			originalBounds[i] = components[i].getBounds();

			components[i].setBounds(absoluteBounds[i]);
		}

		// Updaten
		super.update(delta);

		// Originale Position einsetzten
		for (int i = 0; i < components.length; i++) {
			components[i].setBounds(originalBounds[i]);
		}
	}

	/**
	 * Setzt die absolute Gr��e der Components ein, rendert diese und setzt
	 * anschlie�end wieder die originale Gr��e ein.
	 */
	@Override
	public void render() {
		Component[] components = getComponents();
		Rectangle[] originalBounds = new Rectangle[components.length];

		// Originale Position speichern und absolute Position einsetzen
		for (int i = 0; i < components.length; i++) {
			originalBounds[i] = components[i].getBounds();

			components[i].setBounds(absoluteBounds[i]);
		}

		// Rendern
		super.render();

		// Originale Position einsetzten
		for (int i = 0; i < components.length; i++) {
			components[i].setBounds(originalBounds[i]);
		}
	}

	/**
	 * Berechnet die absolute Position der Components und speichert diese.
	 */
	@Override
	public void scale() {
		Component[] components = getComponents();
		absoluteBounds = new Rectangle[components.length];

		// Geht alle Components durch
		for (int i = 0; i < components.length; i++) {
			// Absolute Position berechnen und speichern
			absoluteBounds[i] = new Rectangle(
					getAbsoluteWidth(components[i].getBounds().x1),
					getAbsoluteHeight(components[i].getBounds().y1),
					getAbsoluteWidth(components[i].getBounds().x2),
					getAbsoluteHeight(components[i].getBounds().y2));
		}
	}

	/**
	 * Gibt die absolute Breite auf dem Bildschirm entsprechend der Fenstergr��e
	 * zur�ck.
	 * 
	 * @param width
	 *            Relative Breite.
	 * @return Absolte Breite.
	 */
	private double getAbsoluteWidth(double width) {
		return width * getWidth() / RELATIVE_WIDTH;
	}

	/**
	 * Gibt die absolute H�he auf dem Bildschirm entsprechend des Fensters
	 * zur�ck.
	 * 
	 * @param height
	 *            Relative H�he.
	 * @return Absolte H�he.
	 */
	private double getAbsoluteHeight(double height) {
		return height * getHeight() / RELATIVE_HEIGHT;
	}
}
