package guiComponents;

/**
 * 
 * Der ActionButton ist ein Button, welcher darauf ausgelegt ist, eine bestimme
 * Aktion (z.B. Ins Hauptmen� gehen) auszuf�hren und diese somit anzeigt.
 * 
 * @author Tobias Schleifstein
 *
 */
public class ActionButton extends Button {
	/**
	 * Beschreibung f�r die Aktion, welche der Button ausf�hren soll und welche
	 * als Titel auf dem Button angezeigt wird.
	 */
	private String action;

	/**
	 * Setzt die Aktion und damit den Titel des Buttons.
	 * 
	 * @param action
	 *            Aktion des Buttons.
	 */
	public void setAction(String action) {
		this.action = action;
		setTitle(action);
	}

	/**
	 * Gibt die Aktion des Buttons zur�ck.
	 * 
	 * @return Aktion des Buttons
	 */
	public String getAction() {
		return action;
	}
}
