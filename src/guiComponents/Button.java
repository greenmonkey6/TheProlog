package guiComponents;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

/**
 * 
 * Der Button ist ein Component, welcher mit der Maus markiert, gedr�ckt oder
 * geklickt werden kann. Die Subklassen reagieren dann entsprechend auf diese
 * Aktionen.
 * 
 * @author Tobias Schleifstein
 * 
 */
public abstract class Button extends Component {
	/** Gibt an, ob sich die Maus auf dem Button befindet. */
	private boolean selected;

	/** Gibt an, ob der Button gedr�ckt ist. */
	private boolean pressed;

	/**
	 * Gibt an, ob der Button klickt ist, also gedr�ckt und wieder losgelassen
	 * wurde.
	 */
	private boolean clicked;

	/** Gibt an, ob der Button gedr�ckt werden kann. */
	private boolean pressable;

	/**
	 * Titel des Buttons, also der String, welcher auf dem Button angezeigt
	 * wird.
	 */
	private String title;

	/**
	 * Initialisiert den Button.
	 */
	public Button() {
		// Button standardm��ig 'dr�ckbar' machen
		pressable = true;

		// Name des Buttons initialisieren
		title = "";
	}

	/**
	 * Aktualisiert den Button und regiert auf die Maus.
	 */
	@Override
	public void update(int delta) {
		// �berpr�fen, ob der Button gedr�ckt werden kann
		if (pressable) {
			// �berpr�fen, ob sich die Maus auf dem Button befindet
			if (Mouse.getX() >= getBounds().x1
					&& Mouse.getX() <= getBounds().x2
					&& Display.getHeight() - Mouse.getY() - 1 >= getBounds().y1
					&& Display.getHeight() - Mouse.getY() - 1 <= getBounds().y2) {
				selected = true;
			} else {
				selected = false;
				pressed = false; // Wenn Button ohne loslassen des Mausbuttons
									// verlassen wird.
			}

			if (selected) {
				// �berpr�fen, ob die Maus gedr�ckt ist
				if (Mouse.isButtonDown(0)) {
					pressed = true;
				} else {
					// Wenn der Button losgelassen wird
					if (pressed) {
						clicked = true;
					}

					pressed = false;
				}
			}
		}
	}

	/**
	 * Rendert den Button.
	 */
	@Override
	public void render() {
		// XXX Muss noch alles relativiert und erweitert werden
		

		// Setzt die Farbe entsprechend des aktuellen Zustands des Buttons
		if (selected) {
			if (pressed) {
				GL11.glColor4d(0.5, 0.5, 0.5, 1);
			} else {
				GL11.glColor4d(0.3, 0.3, 0.3, 1);
			}
		} else {
			GL11.glColor4d(0.25, 0.25, 0.25, 1);
		}

		// Zeichnet Button
		GL11.glRectd(getBounds().x1, getBounds().y1, getBounds().x2,
				getBounds().y2);

		double fontSize = (getBounds().y2 - getBounds().y1) * 0.7;

		Font.renderString(title, (getBounds().x1 + getBounds().x2 - Font
				.getRenderedStringLength(title, fontSize)) * 0.5,
				(getBounds().y1 + getBounds().y2 - fontSize) * 0.5, fontSize,
				new Color(1, 1, 1));
	}

	/**
	 * Gibt zur�ck, ob die Maus �ber dem Button ist.
	 * 
	 * @return Gibt an, ob die Maus �ber dem Button ist.
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Gibt zur�ck, ob der Button gedr�ckt ist.
	 * 
	 * @return Gibt an, ob der Button gedr�ckt ist.
	 */
	public boolean isPressed() {
		return pressed;
	}

	/**
	 * Gibt zur�ck, ob der Button geklickt ist und setzt den Wert danach auf
	 * false.
	 * 
	 * @return Gibt an, ob der Button geklickt ist.
	 */
	public boolean isClicked() {
		boolean temp = clicked;
		clicked = false;

		return temp;
	}

	/**
	 * Setzt, ob der Button gedr�ckt werden kann.
	 * 
	 * @param pressable
	 *            Gibt an, ob der Button gedr�ckt werden k�nnen soll.
	 */
	public void setPressable(boolean pressable) {
		this.pressable = pressable;

		if (!pressable) {
			pressed = false;
		}
	}

	/**
	 * Gibt zur�ck, ob der Button gedr�ckt werden kann.
	 * 
	 * @return Gibt an, ob der Button gedr�ckt werden kann.
	 */
	public boolean isPressable() {
		return pressable;
	}

	/**
	 * Setzt Titel des Buttons.
	 * 
	 * @param title
	 *            Titel des Buttons.
	 */
	protected void setTitle(String title) {
		this.title = title;
	}
}
