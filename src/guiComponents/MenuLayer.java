package guiComponents;

/**
 * 
 * Der MenuLayer ist darauf ausgelegt ein Men� darzustellen, deshalb enth�lt er
 * eine Liste an Buttons, welche automatisch passend skaliert werden.
 * 
 * @author Tobias Schleifstein
 * 
 */
public class MenuLayer extends Layer {
	/** Repr�sentiert den Wert wenn kein Button geklickt ist. */
	public final static int NONE = -1;

	/** Array der Men�-Buttons */
	private ActionButton[] buttons;

	/** Speichert den Button, der zuletzt gedr�ckt wurde. */
	private int currentButton;

	/** Relativer Abstand zwischen den Buttons. */
	private double buttonSpacing;

	/**
	 * Erstellt einen MenuLayer mit Standardeinstellungen.
	 */
	public MenuLayer() {
		// Buttons vorinitialisieren
		buttons = new ActionButton[0];
		currentButton = NONE;

		// Standardeinstellungen festlegen
		buttonSpacing = 1.5;
	}

	/**
	 * Aktualisiert alle Komponenten in der Liste und �berpr�ft, welcher Button
	 * geklickt ist.
	 */
	@Override
	public void update(int delta) {
		super.update(delta);

		// Alle Buttons durchgehen
		for (int i = 0; i < buttons.length; i++) {
			// Wenn der Button geklickt ist
			if (buttons[i].isClicked()) {
				currentButton = i;

				// Schleife verlassen
				i = buttons.length;
			}
		}
	}

	/**
	 * Skaliert die Buttons.
	 */
	@Override
	public void scale() {
		// H�he eines Buttons
		double buttonHeight = getHeight()
				/ (buttons.length + (buttons.length - 1) * (buttonSpacing - 1));

		// Geht alle Buttons durch
		for (int i = 0; i < buttons.length; i++) {
			// Position des aktuellen Buttons in y-Richtung
			double pos = getBounds().y1 + i * buttonHeight * buttonSpacing;

			// Position setzen
			if (buttons[i] != null) {
				buttons[i].setBounds(getBounds().x1, pos, getBounds().x2, pos
						+ buttonHeight);
			}
		}
	}

	/**
	 * Erstellt die Men�-Buttons, anhand eines String-Arrays mit Titel der
	 * Buttons.
	 * 
	 * @param buttonTitles
	 *            Titel der Buttons.
	 */
	public void setButtons(String... buttonTitles) {
		buttons = new ActionButton[buttonTitles.length];

		for (int i = 0; i < buttons.length; i++) {
			buttons[i] = new ActionButton();
			addComponent(buttons[i]);
			buttons[i].setAction(buttonTitles[i]);
		}
	}

	/**
	 * Gibt den Index des aktuellen Buttons im Button-Array zur�ck und setzt
	 * danach den aktuellen Button zur�ck.
	 * 
	 * @return Index des aktuellen Buttons im Button-Array.
	 */
	public int getCurrentButton() {
		int temp = currentButton;
		currentButton = NONE;

		return temp;
	}

	/**
	 * Gibt den Titel des aktuellen Buttons zur�ck und setzt danach den
	 * aktuellen Button zur�ck.
	 * 
	 * @return Titel des aktuellen Buttons.
	 */
	public String getCurrentButtonTitle() {
		if (currentButton == NONE) {
			return null;
		} else {
			String temp = buttons[currentButton].getAction();
			currentButton = NONE;

			return temp;
		}
	}

	/**
	 * Setzt den relativen Abstand zwischen den Buttons.
	 * 
	 * @param buttonSpacing
	 *            Relativer Abstand zwischen den Buttons.
	 */
	public void setButtonSpacing(double buttonSpacing) {
		this.buttonSpacing = buttonSpacing;
	}
}
