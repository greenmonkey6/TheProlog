package guiComponents;

/**
 * 
 * Der StateButton ist ein Button, welcher eine Liste an Status besitzt und
 * einen aktuellen Status, welcher damit angezeigt wird. Diese Status werden als
 * Sting angegeben und werden �ber einen Integer angesteuert. Der aktuelle
 * Status des Buttons ist standardm��ig der erste (bzw. nullte) und wird mit
 * jedem Klick auf diesen erh�ht bzw. beim maximalen Wert zur�ckgesetzt.
 * 
 * @author Tobias Schleifstein
 *
 */
public class StateButton extends Button {
	/**
	 * String, welcher bei Inexistenz eines oder mehrerer Status angezeigt wird.
	 */
	private final String NO_STATE;

	/** Liste an Status. */
	private String[] states;

	/** Aktueller Status aus der Liste an Status. */
	private int currentState;

	/**
	 * Setzt den aktuellen Status des Buttons auf 0 und initialisiert ihn.
	 */
	public StateButton() {
		states = new String[0];
		currentState = 0;

		// Konstante festelegen
		NO_STATE = "NO STATE";
	}

	/**
	 * �berpr�ft, ob der Button gedr�ckt wurde und erh�ht den Status
	 * entsprechend.
	 */
	@Override
	public void update(int delta) {
		super.update(delta);

		// Status erh�hen
		if (isClicked() && states.length > 0) {
			currentState++;

			currentState %= states.length;
		}

		// Titel entsprechend des Status setzten
		if (states.length > 0) {
			setTitle(states[currentState]);
		} else {
			setTitle(NO_STATE);
		}
	}

	/**
	 * Setzt die Status des Buttons.
	 * 
	 * @param states
	 *            Zu setztende Status.
	 */
	public void setStates(String... states) {
		this.states = states;
	}

	/**
	 * Setzt den aktuellen Status des Buttons.
	 * 
	 * @param currentState
	 *            Zu setztender Status des Buttons.
	 */
	public void setCurrentState(int currentState) {
		this.currentState = currentState;
	}

	/**
	 * Gibt den Index des aktuellen Status des Buttons zur�ck.
	 * 
	 * @return Index des aktuellen Status des Buttons.
	 */
	public int getCurrentState() {
		return currentState;
	}

	/**
	 * Setzt den aktuellen Status des Buttons anhand des Namens.
	 * 
	 * @param currentStateName
	 *            Zu setztender Status des Buttons.
	 */
	public void setCurrentStateName(String currentStateName) {
		// Alle Status durchgehen und nach dem richtigen suchen
		for (int i = 0; i < states.length; i++) {
			// Wenn die Status �bereinstimmen
			if (states[i].equals(currentStateName)) {
				currentState = i;

				// Schleife verlassen
				i = states.length;
			}
		}
	}

	/**
	 * Gibt den Namen des aktuellen Status des Buttons zur�ck.
	 * 
	 * @return Name des aktuellen Status des Buttons.
	 */
	public String getCurrentStateName() {
		return states[currentState];
	}
}
