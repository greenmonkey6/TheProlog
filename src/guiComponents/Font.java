package guiComponents;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

/**
 * 
 * Enth�lt statische Methoden zur Darstellung von Schrift.
 * 
 * @author Tobias Schleifstein
 * 
 */
public class Font {
	/** Array, aus String-Array, welchen einen Buchstaben definieren. */
	private static String[][] letters;

	/** Buchstabenh�he in Kacheln. */
	private static final int LETTER_HEIGHT;

	static {
		LETTER_HEIGHT = 7;

		letters = new String[Keyboard.KEYBOARD_SIZE][LETTER_HEIGHT];

		// Buchstaben festlegen
		letters[' '] = new String[] { "  ", "  ", "  ", "  ", "  ", "  ", "  " };
		letters['A'] = new String[] { "0000", "0  0", "0  0", "0000", "0  0",
				"0  0", "0  0" };
		letters['B'] = new String[] { "000 ", "0  0", "0  0", "000 ", "0  0",
				"0  0", "000 " };
		letters['C'] = new String[] { "0000", "0  0", "0   ", "0   ", "0   ",
				"0  0", "0000" };
		letters['D'] = new String[] { "000 ", "0  0", "0  0", "0  0", "0  0",
				"0  0", "000" };
		letters['E'] = new String[] { "0000", "0  0", "0   ", "00  ", "0   ",
				"0  0", "0000" };
		letters['F'] = new String[] { "0000", "0   ", "0   ", "000 ", "0   ",
				"0   ", "0   " };
		letters['G'] = new String[] { "0000", "0  0", "0   ", "0 00", "0  0",
				"0  0", "0000" };
		letters['H'] = new String[] { "0  0", "0  0", "0  0", "0000", "0  0",
				"0  0", "0  0" };
		letters['I'] = new String[] { "000", " 0 ", " 0 ", " 0 ", " 0 ", " 0 ",
				"000" };
		letters['J'] = new String[] { "0000", "0  0", "   0", "   0", "   0",
				"0  0", " 00 " };
		letters['K'] = new String[] { "0  0", "0  0", "0 0 ", "00 ", "0 0 ",
				"0  0", "0  0" };
		letters['L'] = new String[] { "0   ", "0   ", "0   ", "0   ", "0   ",
				"0  0", "0000" };
		letters['M'] = new String[] { "0   0", "00 00", "00 00", "0 0 0",
				"0   0", "0   0", "0   0" };
		letters['N'] = new String[] { "0   0", "00  0", "00  0", "0 0 0",
				"0  00", "0  00", "0   0" };
		letters['O'] = new String[] { "0000", "0  0", "0  0", "0  0", "0  0",
				"0  0", "0000" };
		letters['P'] = new String[] { "0000", "0  0", "0  0", "0000", "0   ",
				"0   ", "0   " };
		letters['Q'] = new String[] { "0000", "0  0", "0  0", "0  0", "0  0",
				"0 00", "0000" };
		letters['R'] = new String[] { "0000", "0  0", "0  0", "0000", "0 0 ",
				"0  0", "0  0" };
		letters['S'] = new String[] { "0000", "0  0", "0   ", "0000", "   0",
				"0  0", "0000" };
		letters['T'] = new String[] { "00000", "0 0 0", "  0  ", "  0  ",
				"  0  ", "  0  ", " 000 " };
		letters['U'] = new String[] { "0  0", "0  0", "0  0", "0  0", "0  0",
				"0  0", "0000" };
		letters['V'] = new String[] { "0   0", "0   0", "0   0", "0   0",
				" 0 0 ", " 0 0 ", "  0  " };
		letters['W'] = new String[] { "0   0", "0   0", "0   0", "0 0 0",
				"00 00", "00 00", "0   0" };
		letters['X'] = new String[] { "0   0", "0   0", " 0 0 ", "  0  ",
				" 0 0 ", "0   0", "0   0" };
		letters['Y'] = new String[] { "0   0", "0   0", "0   0", "00000",
				"  0  ", "  0  ", "  0  " };
		letters['Z'] = new String[] { "00000", "    0", "   0 ", "  0  ",
				" 0   ", "0    ", "00000" };
		letters['0'] = new String[] { "0000", "0  0", "0  0", "0  0", "0  0",
				"0  0", "0000" };
		letters['1'] = new String[] { "  0 ", " 00 ", "  0 ", "  0 ", "  0 ",
				"  0 ", "  0 " };
		letters['2'] = new String[] { "0000", "   0", "   0", "0000", "0   ",
				"0   ", "0000" };
		letters['3'] = new String[] { "0000", "   0", "   0", " 000", "   0",
				"   0", "0000" };
		letters['4'] = new String[] { "0  0", "0  0", "0  0", "0000", "   0",
				"   0", "   0" };
		letters['5'] = new String[] { "0000", "0   ", "0   ", "0000", "   0",
				"   0", "0000" };
		letters['6'] = new String[] { "0000", "0   ", "0   ", "0000", "0  0",
				"0  0", "0000" };
		letters['7'] = new String[] { "0000", "   0", "   0", "   0", "   0",
				"   0", "   0" };
		letters['8'] = new String[] { "0000", "0  0", "0  0", "0000", "0  0",
				"0  0", "0000" };
		letters['9'] = new String[] { "0000", "0  0", "0  0", "0000", "   0",
				"   0", "0000" };
		letters['�'] = new String[] { "0  0", "    ", "0000", "0  0", "0000",
				"0  0", "0  0" };
		letters['�'] = new String[] { "0  0", "    ", "0000", "0  0", "0  0",
				"0  0", "0000" };
		letters['�'] = new String[] { "0  0", "    ", "0  0", "0  0", "0  0",
				"0  0", "0000" };
		letters['!'] = new String[] { "0", "0", "0", "0", "0", " ", "0" };
		letters['-'] = new String[] { "   ", "   ", "   ", "000", "   ", "   ",
				"   " };
		letters['.'] = new String[] { " ", " ", " ", " ", " ", " ", "0" };
		letters['/'] = new String[] { "  0", "  0", " 0 ", " 0 ", " 0 ", "0  ",
				"0  " };
		letters[':'] = new String[] { " ", " ", "0", " ", " ", "0", " " };
		letters['|'] = new String[] { "0", "0", "0", "0", "0", "0", "0" };
	}

	/**
	 * Rendert einen String an einer bestimmten Stelle, in einer bestimmten
	 * Farbe, an einer bestimmten Stelle auf dem Display.
	 * 
	 * @param str
	 *            Zu rendernder String.
	 * @param x
	 *            x-Koordinate der Position, an der der String angezeigt werden
	 *            soll.
	 * @param y
	 *            y-Koordinate der Position, an der der String angezeigt werden
	 *            soll.
	 * @param fontSize
	 *            Schriftgr��e, also H�he der Schrift in pxl.
	 * @param red
	 *            Roter Farbanteil.
	 * @param green
	 *            Gr�ner Farbanteil.
	 * @param blue
	 *            Blauer Farbanteil.
	 * @param alpha
	 *            Transparanz der Farbe.
	 */
	public static void renderString(String str, double x, double y,
			double fontSize, Color color) {
		// Setzt Farbe der Schrift
		GL11.glColor4d(color.red, color.green, color.blue, color.alpha);

		// Gr��e einer Kachel
		double width = fontSize / LETTER_HEIGHT;

		// Cursor f�r den aktuellen Buchstaben
		double cursor = 0;

		// Geht Buchstaben des Strings durch
		for (int i = 0; i < str.length(); i++) {
			// Wenn der Buchstabe in Font existiert
			if (letters[str.charAt(i)][0] != null) {
				// Geht den Buchstaben durch
				for (int j = 0; j < letters[str.charAt(i)].length; j++) {
					for (int k = 0; k < letters[str.charAt(i)][j].length(); k++) {
						if (letters[str.charAt(i)][j].charAt(k) == '0') {
							// Zeichnet die Kachel
							GL11.glRectd(cursor + x + k * width, y + j * width,
									cursor + x + (k + 1) * width, y + (j + 1)
											* width);
						}
					}
				}

				// Bewegt den Cursor weiter
				cursor += (letters[str.charAt(i)][0].length() + 1) * width;
			}
		}
	}

	/**
	 * Gibt die L�nge eines gerenderten Strings in pxl zur�ck.
	 * 
	 * @param str
	 *            Gerendeter String.
	 * @param fontSize
	 *            Gr��e des gerendeten Strings.
	 * @return L�nge des gerenderten Strings.
	 */
	public static double getRenderedStringLength(String str, double fontSize) {
		double length = 0;

		for (int i = 0; i < str.length(); i++) {
			if (letters[str.charAt(i)][0] != null) {
				length += (letters[str.charAt(i)][0].length() + 1) * fontSize
						/ LETTER_HEIGHT;
			}
		}

		length -= fontSize / LETTER_HEIGHT;

		return length;
	}

	/**
	 * Gibt zur�ck, ob ein bestimmter Buchstabe unterst�tzt wird.
	 * 
	 * @param letter
	 *            Zu pr�fender Buchstabe.
	 * @return Gibt an, ob ein der Buchstabe unterst�tzt wird.
	 */
	public static boolean isSupported(char letter) {
		if (letters[letter][0] != null) {
			return true;
		} else {
			return false;
		}
	}
}
