package guiComponents;

import org.lwjgl.input.Keyboard;

/**
 * 
 * Der KeyButton ist darauf ausgelegt eine bestimmte Taste f�r eine Aktion zu
 * belegen und diese beim Klicken zu �ndern.
 * 
 * @author Tobias Schleifstein
 *
 */
public class KeyButton extends Button {
	/** Belegte Taste */
	private int key;

	/** Gibt an, ob sich die Taste gerade �ndert. */
	private boolean changing;

	/**
	 * Reagiert entsprechend auf Druck und Tastatureingabe.
	 */
	@Override
	public void update(int delta) {
		super.update(delta);

		// 'Changing'-Modus einstellen
		if (isClicked()) {
			changing = true;

			// Keyboardspeicher leeren
			while (Keyboard.next())
				;
		}

		// Taste �ndern
		if (changing && Keyboard.next() && Keyboard.getEventKeyState() == true) {
			setKey(Keyboard.getEventKey());
			changing = false;
		}

		// Titel entsprechend des 'Changing'-Modus setzen
		if (changing) {
			setTitle(CursorTimer.getCursor());
		} else {
			setTitle(Keyboard.getKeyName(key));
		}

	}

	/**
	 * Setzt die belegte Taste.
	 * 
	 * @param key
	 *            Belegte Taste.
	 */
	public void setKey(int key) {
		this.key = key;
	}

	/**
	 * Gibt die belegt Taste zur�ck.
	 * 
	 * @return Belegte Taste.
	 */
	public int getKey() {
		return key;
	}
}
