package guiComponents;

import java.util.ArrayList;

/**
 * 
 * Ein Layer ist eine Art Seite oder Kasten, welche auf einem Bildschirm
 * angezeigt werden kann und Components enth�lt, welche er anzeigt.
 * 
 * @author Tobias Schleifstein
 * 
 */
public abstract class Layer extends Component {
	private Layer nextLayer;

	/** Liste mit Components, welche im Layer angezeigt werden. */
	private ArrayList<Component> components;

	/**
	 * Erstellt einen Layer mit leerer Component-Liste.
	 */
	public Layer() {
		components = new ArrayList<Component>();
	}

	/**
	 * Aktualisiert alle Components in der Liste.
	 */
	@Override
	public void update(int delta) {
		// Alle Components updaten
		for (int i = 0; i < components.size(); i++) {
			components.get(i).update(delta);
		}
	}

	/**
	 * Rendert alle Components in der Liste.
	 */
	@Override
	public void render() {
		for (int i = 0; i < components.size(); i++) {
			components.get(i).render();
		}
	}

	/**
	 * Setzt die Boundingbox des Layers und skaliert ihn neu.
	 */
	@Override
	public void setBounds(Rectangle bounds) {
		super.setBounds(bounds);

		// Layer neu skalieren
		scale();
	}

	/**
	 * Skaliert den Layer neu.
	 */
	public abstract void scale();

	/**
	 * F�gt der Liste an Components einen Component hinzu.
	 * 
	 * @param component
	 *            Component, welcher hinzugef�gt werden soll.
	 */
	public void addComponent(Component component) {
		components.add(component);

		// Skaliert den Layer neu
		scale();
	}

	/**
	 * Entfernt der Liste an Components einen Component.
	 * 
	 * @param component
	 *            Component, welcher entfernt werden soll.
	 */
	public void removeComponent(Component component) {
		components.remove(component);

		// Skaliert den Layer neu
		scale();
	}

	/**
	 * Gibt ein Array mit allen Components des Layers zur�ck.
	 * 
	 * @return Array mit allen Components des Layers.
	 */
	protected Component[] getComponents() {
		Object[] objectArray = components.toArray();
		Component[] componentArray = new Component[objectArray.length];

		for (int i = 0; i < componentArray.length; i++) {
			componentArray[i] = (Component) objectArray[i];
		}

		return componentArray;
	}

	protected void setNextLayer(Layer nextLayer) {
		this.nextLayer = nextLayer;
	}

	public Layer getNextLayer() {
		Layer temp = nextLayer;
		nextLayer = null;
		return temp;
	}
}
