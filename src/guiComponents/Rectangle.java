package guiComponents;

/**
 * 
 * Rep�sentiert ein Rechteck, �ber vier Korrdinaten, welche die linke untere und
 * rechte obere Ecke des Rechtecks darstellen.
 * 
 * @author Tobias Schleifstein
 *
 */
public class Rectangle {
	/** x-Koordinate des linken unteren Punktes. */
	double x1;

	/** y-Koordinate des linken unteren Punktes. */
	double y1;

	/** x-Koordinate des rechten oberen Punktes. */
	double x2;

	/** y-Koordinate des rechten oberen Punktes. */
	double y2;

	/**
	 * Erstellt ein Rechteck aus zwei Punkten.
	 * 
	 * @param x1
	 *            x-Koordinate des linken unteren Punktes.
	 * @param y1
	 *            y-Koordinate des linken unteren Punktes.
	 * @param x2
	 *            x-Koordinate des rechten oberen Punktes.
	 * @param y2
	 *            y-Koordinate des rechten oberen Punktes.
	 */
	public Rectangle(double x1, double y1, double x2, double y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	@Override
	public String toString() {
		return String.format("(%.2f|%.2f) (%.2f|%.2f)", x1, y1, x2, y2);
	}
}
