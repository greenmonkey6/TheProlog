package guiComponents;

/**
 * 
 * Der CursorTimer ist eine statische Klasse, welche dazu dient das Blinken
 * eines Cursors zu timen.
 * 
 * @author Tobias Schleifstein
 *
 */
public class CursorTimer {
	/** Gibt an, wie lange der Cursor braucht, um einmal zu blinken. */
	private static long blinkTime;

	/** Zeit, zu der der Cursor das erste Mal benutzt wurde. */
	private static long startTime;

	/** Konstante f�r das Aussehen des Cursors. */
	public static final String CURSOR;

	static {
		// Blinkzeit festlegen.
		blinkTime = 1060;

		// Startzeit festlegen
		startTime = System.currentTimeMillis();

		// Cursor festlegen
		CURSOR = "|";
	}

	/**
	 * Gibt den aktuellen Status des Cursors zur�ck.
	 * 
	 * @return aktueller Status des Cursors.
	 */
	public static boolean getState() {
		return (System.currentTimeMillis() - startTime) % blinkTime < blinkTime * 0.5;
	}

	/**
	 * Gibt den aktuellen Cursor zur�ck
	 * 
	 * @return Aktueller Cursor.
	 */
	public static String getCursor() {
		if (getState() == true) {
			return CURSOR;
		} else {
			return "";
		}
	}
}
