package guiComponents;

/**
 * 
 * Der OnOffButton ist ein Button, welcher einen Status besitzt. Dieser kann die
 * Werte 'ON' bzw. 'OFF' haben, also aus- oder angeschaltet sein. Beim Klicken
 * wechselt der Button seinen Status.
 * 
 * @author Tobias Schleifstein
 *
 */
public class OnOffButton extends Button {
	/** Konstante f�r den 'ON'-Status. */
	private final String ON;

	/** Konstante f�r den 'OFF'-Status. */
	private final String OFF;

	/** Der aktuelle Status des Buttons. */
	private boolean state;

	/**
	 * Setzt die Konstanten und initialisiert den Button.
	 */
	public OnOffButton() {
		ON = "ON";
		OFF = "OFF";
	}

	/**
	 * �berpr�ft, ob der Button geklickt wurde und �ndert den Status und damit
	 * auch den Titel entsprechend.
	 */
	@Override
	public void update(int delta) {
		super.update(delta);

		// Status umschalten
		if (isClicked()) {
			state = !state;
		}

		// Titel entsprechend des Status setzten
		if (state) {
			setTitle(ON);
		} else {
			setTitle(OFF);
		}
	}

	/**
	 * Setzt den Status des Buttons.
	 * 
	 * @param state
	 *            Zu setztender Status des Buttons.
	 */
	public void setState(boolean state) {
		this.state = state;
	}

	/**
	 * Gibt den Status des Buttons zur�ck.
	 * 
	 * @return Status des Buttons.
	 */
	public boolean getState() {
		return state;
	}
}
