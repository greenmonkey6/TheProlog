package guiComponents;

/**
 * 
 * Ein Component ist ein Objekt, welches auf einem Bildschirm dargestellt werden
 * kann. Es muss sich somit darstellen (rendern), sowie aktualisieren k�nnen.
 * Daf�r besitzt es eine Boundingbox, in welcher es angezeigt wird.
 * 
 * @author Tobias Schleifstein
 *
 */
public abstract class Component {
	/** Boundingbox des Components. */
	private Rectangle bounds;

	/**
	 * Erstellt einen Komponenten mit der Gr��e 0.
	 */
	public Component() {
		bounds = new Rectangle(0, 0, 0, 0);
	}

	/**
	 * Aktualisiert den Component.
	 */
	public abstract void update(int delta);

	/**
	 * Rendert den Component.
	 */
	public abstract void render();

	/**
	 * Setzt die Boundingbox des Components.
	 * 
	 * @param bounds
	 *            Boundingbox des Components.
	 */
	public void setBounds(Rectangle bounds) {
		this.bounds = bounds;
	}

	/**
	 * Setzt die Boundingbox des Components. Dabei wird setBounds(Rectangle)
	 * aufgerufen.
	 * 
	 * @param x1
	 *            x-Koordinate des linken unteren Punktes.
	 * @param y1
	 *            y-Koordinate des linken unteren Punktes.
	 * @param x2
	 *            x-Koordinate des rechten oberen Punktes.
	 * @param y2
	 *            y-Koordinate des rechten oberen Punktes.
	 */
	public void setBounds(double x1, double y1, double x2, double y2) {
		setBounds(new Rectangle(x1, y1, x2, y2));
	}

	/**
	 * Gibt die Boundingbox des Components zur�ck.
	 * 
	 * @return Boundingbox des Components.
	 */
	public Rectangle getBounds() {
		return bounds;
	}

	/**
	 * Gibt die Breite des Components zur�ck.
	 * 
	 * @return Breite des Components.
	 */
	public double getWidth() {
		return bounds.x2 - bounds.x1;
	}

	/**
	 * Gibt die H�he des Components zur�ck.
	 * 
	 * @return H�he des Components.
	 */
	public double getHeight() {
		return bounds.y2 - bounds.y1;
	}
}
