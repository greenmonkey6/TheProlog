package guiComponents;

/**
 * 
 * Repr�sentiert eine Farbe mit drei rgb-Werten und einem Alpha-Wert.
 * 
 * @author Tobias Schleifstein
 *
 */
public class Color {
	/** Roter Farbanteil. */
	public double red;

	/** Gr�ner Farbanteil. */
	public double green;

	/** Blauer Farbanteil. */
	public double blue;

	/** Transparanz der Farbe. */
	public double alpha;

	/**
	 * Erstellt eine Farbe aus drei rgb-Werten und l�sst sich standardm��ig
	 * nicht transparant sein.
	 * 
	 * @param red
	 *            Roter Farbanteil.
	 * @param green
	 *            Gr�ner Farbanteil.
	 * @param blue
	 *            Blauer Farbanteil.
	 */
	public Color(double red, double green, double blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		alpha = 1;
	}

	/**
	 * Erstellt eine Farbe aus drei rgb-Werten und einem Alpha-Wert.
	 * 
	 * @param red
	 *            Roter Farbanteil.
	 * @param green
	 *            Gr�ner Farbanteil.
	 * @param blue
	 *            Blauer Farbanteil.
	 * @param alpha
	 *            Transparanz der Farbe.
	 */
	public Color(double red, double green, double blue, double alpha) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}
}
